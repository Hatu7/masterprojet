#include <SFML/Graphics.hpp>
#include "projectiles.hpp"
#include "autre.hpp"
using namespace sf;

void creationProjectileSimple(int nb, Projectile simple[])
{
    simple[nb].position.x = alea(TAILLE_PROJECTILE_SIMPLE, TAILLE_FENETRE - TAILLE_PROJECTILE_SIMPLE);
    simple[nb].position.y = alea(TAILLE_PROJECTILE_SIMPLE, TAILLE_FENETRE- TAILLE_PROJECTILE_SIMPLE);
    simple[nb].couleur = alea(1, 2);
    simple[nb].vitesseX = alea(1,4);
    if (alea(1,2) == 2)
        simple[nb].vitesseX = -simple[nb].vitesseX;
    simple[nb].vitesseY = alea(1,4);
    if (alea(1,2) == 2)
        simple[nb].vitesseY = -simple[nb].vitesseY;
}

void deplacementProjectileSimple (int nb, Projectile simple[])
{
    simple[nb].position.x += simple[nb].vitesseX;
    simple[nb].position.y += simple[nb].vitesseY;
    if (simple[nb].position.x <= 0 || simple[nb].position.x >= TAILLE_FENETRE - TAILLE_PROJECTILE_SIMPLE)
    {
        simple[nb].vitesseX = -simple[nb].vitesseX;
    }
    if (simple[nb].position.y <= 0 || simple[nb].position.y >= TAILLE_FENETRE - TAILLE_PROJECTILE_SIMPLE)
    {
        simple[nb].vitesseY = -simple[nb].vitesseY;
    }
}

void affichageProjectileSimple (int i, Projectile simple[], Clock clock, Sprite *projectileSimple, Texture *imageProjectileSimpleRouge, Texture *imageProjectileSimpleBleu)
{
    if (clock.getElapsedTime().asSeconds() < (i+1) * ECART_SIMPLE + 1.5)
        (*projectileSimple).setColor(Color(255,255,255,128));
    else
    {
        deplacementProjectileSimple(i, simple);
        (*projectileSimple).setColor(Color(255,255,255,255));
    }
    if (simple[i].couleur == 1)
        (*projectileSimple).setTexture(*imageProjectileSimpleRouge);
    else
        (*projectileSimple).setTexture(*imageProjectileSimpleBleu);
    (*projectileSimple).setPosition(simple[i].position.x,simple[i].position.y);
}
