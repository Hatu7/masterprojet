#include <SFML/Graphics.hpp>
#include <time.h>
#include <cmath>
#include "projectiles.hpp"
#include "mouvement.hpp"
#include "autre.hpp"
#include "bonus.hpp"

using namespace sf;
int main()
{
    srand(time(NULL));
    int nbProjectile = 0;
    int vies = 3;
    Projectile simple[50];

    RenderWindow app(VideoMode(TAILLE_FENETRE_X, TAILLE_FENETRE_Y), "Projet");
    app.setFramerateLimit(60);

    Texture imageBackground;
    imageBackground.loadFromFile("inGameBackground.png");
    Sprite background(imageBackground);

    Texture imageProjectileSimpleRouge;
    if (!imageProjectileSimpleRouge.loadFromFile("ProjectileSimpleRouge.png"))
        printf("PB de chargement de l'image !\n");
    Texture imageProjectileSimpleBleu;
    if (!imageProjectileSimpleBleu.loadFromFile("ProjectileSimpleBleu.png"))
        printf("PB de chargement de l'image !\n");
    Sprite projectileSimple;


    Point posVaisseau;
    Texture vaisseau1;
    if (!vaisseau1.loadFromFile("vaisseau1.png"))
        return EXIT_FAILURE;
    Texture vaisseau1Dark;
    if (!vaisseau1Dark.loadFromFile("vaisseau1Dark.png"))
        return EXIT_FAILURE;
    Sprite vaisseau(vaisseau1);
    posVaisseau.x=400;
    posVaisseau.y=300;
    vaisseau.setScale(0.5f,0.5f);
    vaisseau.setOrigin(50,50);
    vaisseau.setPosition(400,300);

    bool bonus1; //Bool�en pour indiqu� si le bonus est allum� ou �teint (True= active et false=d�sactiv�)
    int vitesseV=5;
    Clock clockBonus1; //Clock pour les 5s du bonus
    Clock clock;
    Clock invincibilite;
	Event evenement;
    while (app.isOpen())
    {
        while (app.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                app.close();
                break;
            }
        }
        mouvement(&posVaisseau,vitesseV);
        bonusRalenti(&bonus1,&clockBonus1,&vitesseV); //Regarde si la barre espace est appuyer (Touche du bonus, � changer si besoin dans mouvement.cpp, active le bonus et changer la vitesse, seulement du vaisseau pour le moment)

        app.clear();
        app.draw(background);

        if (vies != 0)
        {
            if (clock.getElapsedTime().asSeconds() >= (nbProjectile+1) * ECART_SIMPLE)
            {
                do
                {
                    creationProjectileSimple(nbProjectile, simple);
                } while (simple[nbProjectile].position.x >= posVaisseau.x - TAILLE_VAISSEAU/2 - 25 && simple[nbProjectile].position.x <= posVaisseau.x + TAILLE_VAISSEAU/2 + 25 &&
                         simple[nbProjectile].position.y <= posVaisseau.y - TAILLE_VAISSEAU/2 - 25 && simple[nbProjectile].position.y >= posVaisseau.y + TAILLE_VAISSEAU/2 + 25);
                nbProjectile++;
            }

            for (int i = 0; i < nbProjectile; i++)
            {
                affichageProjectileSimple (i, vies, bonus1, simple, clock, &projectileSimple, &imageProjectileSimpleRouge, &imageProjectileSimpleBleu);
                app.draw(projectileSimple);
            }
            vaisseau.setPosition(posVaisseau.x,posVaisseau.y);
            checkCollision(nbProjectile, &vies, simple, posVaisseau, clock, &invincibilite);
            if (invincibilite.getElapsedTime().asSeconds() < 2 && clock.getElapsedTime().asSeconds() > 2 && (std::fmod(invincibilite.getElapsedTime().asSeconds(), 0.25)) < 0.125)
                vaisseau.setTexture(vaisseau1Dark);
            else
                vaisseau.setTexture(vaisseau1);
            app.draw(vaisseau);
        }

        if (vies == 0)
        {
            for (int i = 0; i < nbProjectile; i++)
            {
                affichageProjectileSimple (i, vies, bonus1, simple, clock, &projectileSimple, &imageProjectileSimpleRouge, &imageProjectileSimpleBleu);
                app.draw(projectileSimple);
            }
            app.draw(vaisseau);

            //menu rejouer/quitter
        }

        app.display();
        printf("%f %i\n",invincibilite.getElapsedTime().asSeconds(), bonus1);
    }

    return EXIT_SUCCESS;
}
