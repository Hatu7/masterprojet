#include <SFML/Graphics.hpp>
#include "bonus.hpp"
using namespace sf;

void bonusRalenti(bool * bonus,sf::Clock * clockBonus, int * vitesseVaisseau) //A compléter avec le ralentissement de vitesse des projectiles, comme la vitesse est un int j'ai mis 5 et 2 au lieu de faire un vitesse/2
{
    if (Keyboard::isKeyPressed(Keyboard::Space))
    {
        *bonus=true;
        clockBonus->restart();
    }
    if (clockBonus->getElapsedTime().asSeconds() <= 5 && *bonus==true)
        *vitesseVaisseau=2;
    else
    {
        *vitesseVaisseau=5;
        *bonus=false;
    }
}
