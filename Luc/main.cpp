#include <SFML/Graphics.hpp>
#include <time.h>
#include "projectiles.hpp"
#include "autre.hpp"

using namespace sf;
int main()
{
    srand(time(NULL));
    int nbProjectile = 0;
    Projectile simple[50];

    RenderWindow app(VideoMode(TAILLE_FENETRE, TAILLE_FENETRE), "Projet");
    app.setFramerateLimit(60);

    Texture imageProjectileSimpleRouge;
    imageProjectileSimpleRouge.loadFromFile("ProjectileSimpleRouge.png");
    Texture imageProjectileSimpleBleu;
    imageProjectileSimpleBleu.loadFromFile("ProjectileSimpleBleu.png");

    Sprite projectileSimple;

    Clock clock;
	Event evenement;
    while (app.isOpen())
    {
        while (app.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                app.close();
                break;
            }
        }

        app.clear();
        if (clock.getElapsedTime().asSeconds() >= (nbProjectile+1) * ECART_SIMPLE)
        {
            creationProjectileSimple(nbProjectile, simple);
            nbProjectile++;
        }

        for (int i = 0; i < nbProjectile; i++)
        {
            affichageProjectileSimple (i, simple, clock, &projectileSimple, &imageProjectileSimpleRouge, &imageProjectileSimpleBleu);
            app.draw(projectileSimple);
        }
        app.display();
    }

    return EXIT_SUCCESS;
}
