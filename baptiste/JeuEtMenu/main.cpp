#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string.h>
#include <iostream>

#define LARGEUR_FENETRE 1200
#define HAUTEUR_FENETRE 800
#define LARGEUR_LOGO 900
#define HAUTEUR_LOGO 300
#define LARGEUR_BOUTON 450
#define HAUTEUR_BOUTON 150
#define TAILLE_BOUTONCARRE 105
#define VIDE_BUTTON 27
#define TAILLE_TEXTE_DESC 25
#define TAILLE_TEXTE_MENU 50
#define TAILLE_TEXTE_TITRE 100
#define TAILLE_VAISSEAU 200
#define LARGEUR_BARRE 1001
#define HAUTEUR_BARRE 51
#define TAILLE_PETITBOUTON 55
#define TAILLE_TEXTE_TITREBARRE 40
#define TAILLE_TEXTE_TITRELONGBARRE 30
#define TAILLE_TEXTE_SMALLTEXTEBARRE 20
#define TAILLE_ROND 84

using namespace sf;
using namespace std;

typedef struct
{
    int id;
    char nom[30];

} Vaisseau;

typedef struct
{
    Vaisseau vaisseau;
    char nom[30];

} Joueur;

int menu = 1;
int soundState = 1;
int volume = 25;
const char *NOM_VAISSEAU[5] = {"Starship", "Eagleship", "Discoveryship", "Prawnship", "Nostroship"};
int scores[3] = {47823, 22156, 18453};

int difficulte = 3;
int score = 54140;
int vies = 2;
int bonus[3] = {0,1,0};
int gameover = 0;

int main()
{
    // Main menu window

    RenderWindow window(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "SPACE ESCAPE");

    // Initialiser les param�tres de base
    Joueur joueur;
    strcpy(joueur.nom, "Joueur");
    joueur.vaisseau.id = 1;
    strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[joueur.vaisseau.id-1]);

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////                                   MENU DU                                      ////
    ////                                     JEU                                        ////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    // Texture for the main menu

    Texture background;
    Texture logo;
    Texture button;
    Texture buttonPressed;
    Texture trophyButton;
    Texture trophyButtonPressed;

    Texture bar;
    Texture bar25;
    Texture bar50;
    Texture bar75;
    Texture bar100;
    Texture back;
    Texture forward;
    Texture backPressed;
    Texture forwardPressed;

    Texture vaisseau1;
    Texture vaisseau2;
    Texture vaisseau3;
    Texture vaisseau4;
    Texture vaisseau5;
    Texture vaisseau1Dark;
    Texture vaisseau2Dark;
    Texture vaisseau3Dark;
    Texture vaisseau4Dark;
    Texture vaisseau5Dark;
    Texture vaisseau1Choice;
    Texture vaisseau2Choice;
    Texture vaisseau3Choice;
    Texture vaisseau4Choice;
    Texture vaisseau5Choice;

    Texture round;
    Texture roundPressed;

    // Load Files

    background.loadFromFile("background.png");
    logo.loadFromFile("logo.png");
    button.loadFromFile("button.png");
    buttonPressed.loadFromFile("buttonPressed.png");
    vaisseau1.loadFromFile("vaisseau1.png");
    vaisseau2.loadFromFile("vaisseau2.png");
    vaisseau3.loadFromFile("vaisseau3.png");
    vaisseau4.loadFromFile("vaisseau4.png");
    vaisseau5.loadFromFile("vaisseau5.png");
    vaisseau1Dark.loadFromFile("vaisseau1Dark.png");
    vaisseau2Dark.loadFromFile("vaisseau2Dark.png");
    vaisseau3Dark.loadFromFile("vaisseau3Dark.png");
    vaisseau4Dark.loadFromFile("vaisseau4Dark.png");
    vaisseau5Dark.loadFromFile("vaisseau5Dark.png");
    vaisseau1Choice.loadFromFile("vaisseau1Choice.png");
    vaisseau2Choice.loadFromFile("vaisseau2Choice.png");
    vaisseau3Choice.loadFromFile("vaisseau3Choice.png");
    vaisseau4Choice.loadFromFile("vaisseau4Choice.png");
    vaisseau5Choice.loadFromFile("vaisseau5Choice.png");
    trophyButton.loadFromFile("trophy.png");
    trophyButtonPressed.loadFromFile("trophyPressed.png");
    bar.loadFromFile("bar.png");
    bar25.loadFromFile("bar25.png");
    bar50.loadFromFile("bar50.png");
    bar75.loadFromFile("bar75.png");
    bar100.loadFromFile("bar100.png");
    back.loadFromFile("back.png");
    forward.loadFromFile("forward.png");
    backPressed.loadFromFile("backPressed.png");
    forwardPressed.loadFromFile("forwardPressed.png");
    round.loadFromFile("round01.png");
    roundPressed.loadFromFile("round01Pressed.png");

    // Sprites declaration

    Sprite backgroundSprite(background);
    Sprite logoSprite;
    Sprite startButtonImage;
    Sprite optionsButtonImage;
    Sprite quitButtonImage;
    Sprite vaisseauSprite;
    Sprite trophySprite;

    Sprite backButtonImage;
    Sprite barSprite;
    Sprite backSprite;
    Sprite forwardSprite;

    Sprite vaisseau1Sprite;
    Sprite vaisseau2Sprite;
    Sprite vaisseau3Sprite;
    Sprite vaisseau4Sprite;
    Sprite vaisseau5Sprite;
    Sprite validationButtonImage;

    Sprite difficulte1Sprite;
    Sprite difficulte2Sprite;
    Sprite difficulte3Sprite;

    // Setting the texture for the sprites

    logoSprite.setTexture(logo);
    startButtonImage.setTexture(button);
    optionsButtonImage.setTexture(button);
    quitButtonImage.setTexture(button);
    trophySprite.setTexture(trophyButton);
    backButtonImage.setTexture(button);
    backSprite.setTexture(back);
    forwardSprite.setTexture(forward);
    difficulte1Sprite.setTexture(round);
    difficulte2Sprite.setTexture(round);
    difficulte3Sprite.setTexture(round);

    switch(volume)
    {
    case 0:
        barSprite.setTexture(bar);
        break;
    case 25:
        barSprite.setTexture(bar25);
        break;
    case 50:
        barSprite.setTexture(bar50);
        break;
    case 75:
        barSprite.setTexture(bar75);
        break;
    case 100:
        barSprite.setTexture(bar100);
        break;
    }

    vaisseau1Sprite.setTexture(vaisseau1Dark);
    vaisseau2Sprite.setTexture(vaisseau2Dark);
    vaisseau3Sprite.setTexture(vaisseau3Dark);
    vaisseau4Sprite.setTexture(vaisseau4Dark);
    vaisseau5Sprite.setTexture(vaisseau5Dark);
    validationButtonImage.setTexture(button);

    switch(joueur.vaisseau.id)
    {
    case 1:
        vaisseauSprite.setTexture(vaisseau1);
        vaisseau1Sprite.setTexture(vaisseau1Choice);
        break;
    case 2:
        vaisseauSprite.setTexture(vaisseau2);
        vaisseau2Sprite.setTexture(vaisseau2Choice);
        break;
    case 3:
        vaisseauSprite.setTexture(vaisseau3);
        vaisseau3Sprite.setTexture(vaisseau3Choice);
        break;
    case 4:
        vaisseauSprite.setTexture(vaisseau4);
        vaisseau4Sprite.setTexture(vaisseau4Choice);
        break;
    case 5:
        vaisseauSprite.setTexture(vaisseau5);
        vaisseau5Sprite.setTexture(vaisseau5Choice);
        break;
    }

    // Setting space and position for the sprites
    ////////////////////////////////////////////////////////////////////////////////////////

    logoSprite.setOrigin(LARGEUR_LOGO/2, HAUTEUR_LOGO/2);
    logoSprite.setPosition(LARGEUR_FENETRE/2, 0.02*HAUTEUR_FENETRE+HAUTEUR_LOGO/2);

    startButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    startButtonImage.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);

    optionsButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    optionsButtonImage.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON);

    quitButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    quitButtonImage.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);

    vaisseauSprite.setOrigin(TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2);
    vaisseauSprite.setPosition(0.1*LARGEUR_FENETRE, HAUTEUR_FENETRE-TAILLE_VAISSEAU+0.10*HAUTEUR_FENETRE);

    trophySprite.setOrigin(TAILLE_BOUTONCARRE/2, TAILLE_BOUTONCARRE/2);
    trophySprite.setPosition(LARGEUR_FENETRE-TAILLE_BOUTONCARRE/2, HAUTEUR_FENETRE-TAILLE_BOUTONCARRE/2);

    ////////////////////////////////////////////////////////////////////////////////////////

    vaisseau1Sprite.setOrigin(TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2);
    vaisseau1Sprite.setPosition(LARGEUR_FENETRE/2 - 2*(TAILLE_VAISSEAU) - 2*(LARGEUR_FENETRE/30), HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);

    vaisseau2Sprite.setOrigin(TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2);
    vaisseau2Sprite.setPosition(LARGEUR_FENETRE/2 - TAILLE_VAISSEAU - LARGEUR_FENETRE/30, HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);

    vaisseau3Sprite.setOrigin(TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2);
    vaisseau3Sprite.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);

    vaisseau4Sprite.setOrigin(TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2);
    vaisseau4Sprite.setPosition(LARGEUR_FENETRE/2 + TAILLE_VAISSEAU + LARGEUR_FENETRE/30, HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);

    vaisseau5Sprite.setOrigin(TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2);
    vaisseau5Sprite.setPosition(LARGEUR_FENETRE/2 + 2*(TAILLE_VAISSEAU) + 2*(LARGEUR_FENETRE/30), HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);

    validationButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    validationButtonImage.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE - HAUTEUR_BOUTON + HAUTEUR_FENETRE/10);

    ////////////////////////////////////////////////////////////////////////////////////////

    backButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    backButtonImage.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);

    barSprite.setOrigin(LARGEUR_BARRE/2, HAUTEUR_BARRE/2);
    barSprite.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BARRE*2);

    backSprite.setOrigin(TAILLE_PETITBOUTON/2, TAILLE_PETITBOUTON/2);
    backSprite.setPosition(TAILLE_PETITBOUTON, barSprite.getPosition().y);

    forwardSprite.setOrigin(TAILLE_PETITBOUTON/2, TAILLE_PETITBOUTON/2);
    forwardSprite.setPosition(LARGEUR_FENETRE-TAILLE_PETITBOUTON, barSprite.getPosition().y);

    ////////////////////////////////////////////////////////////////////////////////////////

    difficulte1Sprite.setOrigin(TAILLE_ROND/2, TAILLE_ROND/2);
    difficulte1Sprite.setPosition(LARGEUR_FENETRE/3, HAUTEUR_FENETRE/1.5);
    difficulte1Sprite.setScale(Vector2f(2, 2));

    difficulte2Sprite.setOrigin(TAILLE_ROND/2, TAILLE_ROND/2);
    difficulte2Sprite.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/1.5);
    difficulte2Sprite.setScale(Vector2f(2, 2));

    difficulte3Sprite.setOrigin(TAILLE_ROND/2, TAILLE_ROND/2);
    difficulte3Sprite.setPosition(LARGEUR_FENETRE-LARGEUR_FENETRE/3, HAUTEUR_FENETRE/1.5);
    difficulte3Sprite.setScale(Vector2f(2, 2));

    ////////////////////////////////////////////////////////////////////////////////////////
    // Texte
    ////////////////////////////////////////////////////////////////////////////////////////

    Font font;
    font.loadFromFile("SWEETMANGO.ttf");
    Font fontSpace;
    fontSpace.loadFromFile("SPACE.ttf");

    Text startText;
    startText.setFont(font);
    startText.setString("Jouer");
    startText.setCharacterSize(TAILLE_TEXTE_MENU);
    startText.setFillColor(Color::White);

    startText.setOrigin(startText.getLocalBounds().width/2, startText.getLocalBounds().height);
    startText.setPosition(startButtonImage.getPosition());

    Text optionsText;
    optionsText.setFont(font);
    optionsText.setString("Options");
    optionsText.setCharacterSize(TAILLE_TEXTE_MENU);
    optionsText.setFillColor(Color::White);

    optionsText.setOrigin(optionsText.getLocalBounds().width/2, optionsText.getLocalBounds().height/1.25);
    optionsText.setPosition(optionsButtonImage.getPosition());

    Text quitText;
    quitText.setFont(font);
    quitText.setString("Quitter");
    quitText.setCharacterSize(TAILLE_TEXTE_MENU);
    quitText.setFillColor(Color::White);

    quitText.setOrigin(quitText.getLocalBounds().width/2, quitText.getLocalBounds().height);
    quitText.setPosition(quitButtonImage.getPosition());

    ////////////////////////////////////////////////////////////////////////////////////////

    Text choixVaisseauText;
    choixVaisseauText.setFont(font);
    choixVaisseauText.setString("Choisissez votre vaisseau");
    choixVaisseauText.setCharacterSize(TAILLE_TEXTE_TITRE);
    choixVaisseauText.setFillColor(Color::White);

    choixVaisseauText.setOrigin(choixVaisseauText.getLocalBounds().width/2, choixVaisseauText.getLocalBounds().height);
    choixVaisseauText.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE*0.95/2);

    Text validationText;
    validationText.setFont(font);
    validationText.setString("Valider");
    validationText.setCharacterSize(TAILLE_TEXTE_MENU);
    validationText.setFillColor(Color::White);

    validationText.setOrigin(validationText.getLocalBounds().width/2, validationText.getLocalBounds().height);
    validationText.setPosition(validationButtonImage.getPosition());

    Text nomVaisseauText;
    nomVaisseauText.setFont(font);
    nomVaisseauText.setString(NOM_VAISSEAU[joueur.vaisseau.id-1]);
    nomVaisseauText.setCharacterSize(TAILLE_TEXTE_DESC);
    nomVaisseauText.setFillColor(Color::White);

    nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x - nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y);
    ////////////////////////////////////////////////////////////////////////////////////////

    Text backText;
    backText.setFont(font);
    backText.setString("Retour");
    backText.setCharacterSize(TAILLE_TEXTE_MENU);
    backText.setFillColor(Color::White);

    backText.setOrigin(backText.getLocalBounds().width/2, backText.getLocalBounds().height);
    backText.setPosition(backButtonImage.getPosition());

    Text volumeChoiceText;
    volumeChoiceText.setFont(font);
    volumeChoiceText.setString("Volume");
    volumeChoiceText.setCharacterSize(TAILLE_TEXTE_TITRE);
    volumeChoiceText.setFillColor(Color::White);

    volumeChoiceText.setOrigin(volumeChoiceText.getLocalBounds().width/2, volumeChoiceText.getLocalBounds().height);
    volumeChoiceText.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE*0.95/2);

    Text volumeNumberText;
    volumeNumberText.setFont(font);
    volumeNumberText.setString("Volume");
    volumeNumberText.setCharacterSize(TAILLE_TEXTE_MENU);
    volumeNumberText.setFillColor(Color::White);

    volumeNumberText.setOrigin(volumeNumberText.getLocalBounds().width/2, volumeNumberText.getLocalBounds().height);
    volumeNumberText.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/1.585);

    ////////////////////////////////////////////////////////////////////////////////////////

    Text scoresText;
    scoresText.setFont(font);
    scoresText.setString("Meilleurs scores");
    scoresText.setCharacterSize(TAILLE_TEXTE_MENU);
    scoresText.setFillColor(Color::White);

    scoresText.setOrigin(scoresText.getLocalBounds().width/2, scoresText.getLocalBounds().height);
    scoresText.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2.4);

    Text score1Text;
    score1Text.setFont(font);
    score1Text.setCharacterSize(TAILLE_TEXTE_MENU);
    score1Text.setFillColor(Color::White);

    score1Text.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);

    Text score2Text;
    score2Text.setFont(font);
    score2Text.setCharacterSize(TAILLE_TEXTE_MENU);
    score2Text.setFillColor(Color::White);

    score2Text.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+50);

    Text score3Text;
    score3Text.setFont(font);
    score3Text.setCharacterSize(TAILLE_TEXTE_MENU);
    score3Text.setFillColor(Color::White);

    score3Text.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+100);

    ////////////////////////////////////////////////////////////////////////////////////////
    Text difficulteChoiceText;
    difficulteChoiceText.setFont(font);
    difficulteChoiceText.setString("Choix de la difficulte");
    difficulteChoiceText.setCharacterSize(TAILLE_TEXTE_MENU);
    difficulteChoiceText.setFillColor(Color::White);

    difficulteChoiceText.setOrigin(difficulteChoiceText.getLocalBounds().width/2, difficulteChoiceText.getLocalBounds().height);
    difficulteChoiceText.setPosition(LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);

    Text difficulte1Text;
    difficulte1Text.setFont(fontSpace);
    difficulte1Text.setString("1");
    difficulte1Text.setCharacterSize(TAILLE_TEXTE_TITRE);
    difficulte1Text.setFillColor(Color::White);

    difficulte1Text.setOrigin(difficulte1Text.getLocalBounds().width/1.6, difficulte1Text.getLocalBounds().height/1.5);
    difficulte1Text.setPosition(difficulte1Sprite.getPosition().x, difficulte1Sprite.getPosition().y);

    Text difficulte2Text;
    difficulte2Text.setFont(fontSpace);
    difficulte2Text.setString("2");
    difficulte2Text.setCharacterSize(TAILLE_TEXTE_TITRE);
    difficulte2Text.setFillColor(Color::White);

    difficulte2Text.setOrigin(difficulte2Text.getLocalBounds().width/1.8, difficulte2Text.getLocalBounds().height/1.4);
    difficulte2Text.setPosition(difficulte2Sprite.getPosition().x, difficulte2Sprite.getPosition().y);

    Text difficulte3Text;
    difficulte3Text.setFont(fontSpace);
    difficulte3Text.setString("3");
    difficulte3Text.setCharacterSize(TAILLE_TEXTE_TITRE);
    difficulte3Text.setFillColor(Color::White);

    difficulte3Text.setOrigin(difficulte3Text.getLocalBounds().width/1.8, difficulte3Text.getLocalBounds().height/1.4);
    difficulte3Text.setPosition(difficulte3Sprite.getPosition().x, difficulte3Sprite.getPosition().y);

    ////////////////////////////////////////////////////////////////////////////////////////
    // Son
    SoundBuffer buffer;
    buffer.loadFromFile("music.wav");

    Sound sound;
    sound.setBuffer(buffer);
    sound.setVolume(volume);
    sound.play();

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////                                                                                ////
    ////                                     JEU                                        ////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    Texture inGamebackground;
    inGamebackground.loadFromFile("inGameBackground.png");
    Sprite inGamebackgroundSprite(inGamebackground);

    // BARRE A DROITE //
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text titleText;
    titleText.setFont(fontSpace);
    titleText.setString("Infos");
    titleText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    titleText.setFillColor(Color::White);
    titleText.setOrigin(titleText.getLocalBounds().width/2, titleText.getLocalBounds().height);
    titleText.setPosition(1050,52);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text difficulteText;
    difficulteText.setFont(fontSpace);
    difficulteText.setString("Difficulte");
    difficulteText.setCharacterSize(TAILLE_TEXTE_TITRELONGBARRE);
    difficulteText.setFillColor(Color::White);
    difficulteText.setOrigin(difficulteText.getLocalBounds().width/2, difficulteText.getLocalBounds().height);
    difficulteText.setPosition(1050,130);

    Texture round01;
    round01.loadFromFile("round01.png");

    Sprite levelRoundSprite;
    levelRoundSprite.setTexture(round01);
    levelRoundSprite.setOrigin(levelRoundSprite.getLocalBounds().width/2, levelRoundSprite.getLocalBounds().height);
    levelRoundSprite.setPosition(1050,235);

    Text difficulteNumberText;
    difficulteNumberText.setFont(fontSpace);
    difficulteNumberText.setPosition(1047,200);
    difficulteNumberText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    difficulteNumberText.setFillColor(Color::White);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text scoreText;
    scoreText.setFont(fontSpace);
    scoreText.setString("Score");
    scoreText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    scoreText.setFillColor(Color::White);
    scoreText.setOrigin(scoreText.getLocalBounds().width/2, scoreText.getLocalBounds().height);
    scoreText.setPosition(1050,290);

    Texture box01;
    box01.loadFromFile("box01.png");

    Sprite scoreBoxSprite;
    scoreBoxSprite.setTexture(box01);
    scoreBoxSprite.setOrigin(scoreBoxSprite.getLocalBounds().width/2, scoreBoxSprite.getLocalBounds().height);
    scoreBoxSprite.setPosition(1050,360);

    Text scoreNumberText;
    scoreNumberText.setFont(fontSpace);
    scoreNumberText.setPosition(1050,342);
    scoreNumberText.setCharacterSize(TAILLE_TEXTE_SMALLTEXTEBARRE);
    scoreNumberText.setFillColor(Color::White);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text viesText;
    viesText.setFont(fontSpace);
    viesText.setString("Vies");
    viesText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    viesText.setFillColor(Color::White);
    viesText.setOrigin(viesText.getLocalBounds().width/2, viesText.getLocalBounds().height);
    viesText.setPosition(1050,410);

    Texture starEmpty;
    starEmpty.loadFromFile("starEmpty.png");
    Texture star;
    star.loadFromFile("star.png");

    Sprite starEmpty1;
    starEmpty1.setTexture(starEmpty);
    starEmpty1.setOrigin(starEmpty1.getLocalBounds().width/2, starEmpty1.getLocalBounds().height);
    starEmpty1.setPosition(965,515);

    Sprite starEmpty2;
    starEmpty2.setTexture(starEmpty);
    starEmpty2.setOrigin(starEmpty2.getLocalBounds().width/2, starEmpty2.getLocalBounds().height);
    starEmpty2.setPosition(1050,515);

    Sprite starEmpty3;
    starEmpty3.setTexture(starEmpty);
    starEmpty3.setOrigin(starEmpty3.getLocalBounds().width/2, starEmpty3.getLocalBounds().height);
    starEmpty3.setPosition(1135,515);

    Sprite star1;
    star1.setTexture(star);
    star1.setOrigin(star1.getLocalBounds().width/2, star1.getLocalBounds().height);
    star1.setPosition(965,515);

    Sprite star2;
    star2.setTexture(star);
    star2.setOrigin(star2.getLocalBounds().width/2, star2.getLocalBounds().height);
    star2.setPosition(1050,515);

    Sprite star3;
    star3.setTexture(star);
    star3.setOrigin(star3.getLocalBounds().width/2, star3.getLocalBounds().height);
    star3.setPosition(1135,515);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text bonusText;
    bonusText.setFont(fontSpace);
    bonusText.setString("Bonus");
    bonusText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    bonusText.setFillColor(Color::White);
    bonusText.setOrigin(bonusText.getLocalBounds().width/2, bonusText.getLocalBounds().height);
    bonusText.setPosition(1050,565);

    Texture round02;
    round02.loadFromFile("round02.png");

    Sprite bonus1RoundSprite;
    bonus1RoundSprite.setTexture(round02);
    bonus1RoundSprite.setOrigin(bonus1RoundSprite.getLocalBounds().width/2, bonus1RoundSprite.getLocalBounds().height);
    bonus1RoundSprite.setPosition(965,680);

    Sprite bonus2RoundSprite;
    bonus2RoundSprite.setTexture(round02);
    bonus2RoundSprite.setOrigin(bonus2RoundSprite.getLocalBounds().width/2, bonus2RoundSprite.getLocalBounds().height);
    bonus2RoundSprite.setPosition(1050,680);

    Sprite bonus3RoundSprite;
    bonus3RoundSprite.setTexture(round02);
    bonus3RoundSprite.setOrigin(bonus3RoundSprite.getLocalBounds().width/2, bonus3RoundSprite.getLocalBounds().height);
    bonus3RoundSprite.setPosition(1135,680);

    Texture bonus1Time;
    bonus1Time.loadFromFile("bonus1Time.png");
    Texture bonus2Bomb;
    bonus2Bomb.loadFromFile("bonus2Bomb.png");
    Texture bonus3Speed;
    bonus3Speed.loadFromFile("bonus3Speed.png");
    Texture bonus1TimeEmpty;
    bonus1TimeEmpty.loadFromFile("bonus1TimeEmpty.png");
    Texture bonus2BombEmpty;
    bonus2BombEmpty.loadFromFile("bonus2BombEmpty.png");
    Texture bonus3SpeedEmpty;
    bonus3SpeedEmpty.loadFromFile("bonus3SpeedEmpty.png");

    Sprite bonus1Sprite;
    bonus1Sprite.setTexture(bonus1Time);
    bonus1Sprite.setOrigin(bonus1Sprite.getLocalBounds().width/2, bonus1Sprite.getLocalBounds().height);
    bonus1Sprite.setScale(Vector2f(0.8, 0.8));
    bonus1Sprite.setPosition(965,662);

    Sprite bonus2Sprite;
    bonus2Sprite.setTexture(bonus2Bomb);
    bonus2Sprite.setOrigin(bonus2Sprite.getLocalBounds().width/2, bonus2Sprite.getLocalBounds().height);
    bonus2Sprite.setScale(Vector2f(0.8, 0.8));
    bonus2Sprite.setPosition(1050,662);

    Sprite bonus3Sprite;
    bonus3Sprite.setTexture(bonus3Speed);
    bonus3Sprite.setOrigin(bonus3Sprite.getLocalBounds().width/2, bonus3Sprite.getLocalBounds().height);
    bonus3Sprite.setScale(Vector2f(0.8, 0.8));
    bonus3Sprite.setPosition(1135,659);

    Sprite bonus1SpriteEmpty;
    bonus1SpriteEmpty.setTexture(bonus1TimeEmpty);
    bonus1SpriteEmpty.setOrigin(bonus1SpriteEmpty.getLocalBounds().width/2, bonus1SpriteEmpty.getLocalBounds().height);
    bonus1SpriteEmpty.setScale(Vector2f(0.8, 0.8));
    bonus1SpriteEmpty.setPosition(965,662);

    Sprite bonus2SpriteEmpty;
    bonus2SpriteEmpty.setTexture(bonus2BombEmpty);
    bonus2SpriteEmpty.setOrigin(bonus2SpriteEmpty.getLocalBounds().width/2, bonus2SpriteEmpty.getLocalBounds().height);
    bonus2SpriteEmpty.setScale(Vector2f(0.8, 0.8));
    bonus2SpriteEmpty.setPosition(1050,662);

    Sprite bonus3SpriteEmpty;
    bonus3SpriteEmpty.setTexture(bonus3SpeedEmpty);
    bonus3SpriteEmpty.setOrigin(bonus3SpriteEmpty.getLocalBounds().width/2, bonus3SpriteEmpty.getLocalBounds().height);
    bonus3SpriteEmpty.setScale(Vector2f(0.8, 0.8));
    bonus3SpriteEmpty.setPosition(1135,659);

    //////////////////////////////////////////////////////////////////////////////////////////////
    // GAMEOVER

    Text gameOverText;
    gameOverText.setFont(fontSpace);
    gameOverText.setString("GAME OVER");
    gameOverText.setCharacterSize(TAILLE_TEXTE_TITRE);
    gameOverText.setFillColor(Color::White);

    gameOverText.setOrigin(gameOverText.getLocalBounds().width/2, gameOverText.getLocalBounds().height);
    gameOverText.setPosition(LARGEUR_FENETRE/2.7, HAUTEUR_FENETRE*0.95/2);

    Text gameOverScoreText;
    gameOverScoreText.setFont(fontSpace);
    gameOverScoreText.setCharacterSize(TAILLE_TEXTE_MENU);
    gameOverScoreText.setFillColor(Color::White);

    gameOverScoreText.setPosition(LARGEUR_FENETRE/2.7, HAUTEUR_FENETRE/1.7);

    Sprite returnToMenuButtonImage;
    returnToMenuButtonImage.setTexture(button);

    returnToMenuButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    returnToMenuButtonImage.setPosition(LARGEUR_FENETRE/5, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);

    Text returnToMenuText;
    returnToMenuText.setFont(font);
    returnToMenuText.setString("Retour au menu");
    returnToMenuText.setCharacterSize(TAILLE_TEXTE_MENU);
    returnToMenuText.setFillColor(Color::White);

    returnToMenuText.setOrigin(returnToMenuText.getLocalBounds().width/2, returnToMenuText.getLocalBounds().height);
    returnToMenuText.setPosition(returnToMenuButtonImage.getPosition());

    Sprite playAgainButtonImage;
    playAgainButtonImage.setTexture(button);

    playAgainButtonImage.setOrigin(LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2);
    playAgainButtonImage.setPosition(LARGEUR_FENETRE/5+LARGEUR_BOUTON*0.95, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);

    Text playAgainText;
    playAgainText.setFont(font);
    playAgainText.setString("Rejouer");
    playAgainText.setCharacterSize(TAILLE_TEXTE_MENU);
    playAgainText.setFillColor(Color::White);

    playAgainText.setOrigin(playAgainText.getLocalBounds().width/2, playAgainText.getLocalBounds().height);
    playAgainText.setPosition(playAgainButtonImage.getPosition());

    //////////////////////////////////////////////////////////////////////////////////////////////

    // Main loop

    while(window.isOpen())
    {
        Event event;
        while(window.pollEvent(event))
        {
            if(event.type == Event::Closed)
                window.close();
            if (event.type == Event::MouseMoved)
            {
                if(menu == 1) // SI ON EST DANS LE MENU PRINCIPAL
                {
                    if(event.mouseMove.x >= startButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= startButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= startButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= startButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        startButtonImage.setTexture(buttonPressed);
                        startText.setPosition(startButtonImage.getPosition().x+2, startButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        startButtonImage.setTexture(button);
                        startText.setPosition(startButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= optionsButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= optionsButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= optionsButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= optionsButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        optionsButtonImage.setTexture(buttonPressed);
                        optionsText.setPosition(optionsButtonImage.getPosition().x+2, optionsButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        optionsButtonImage.setTexture(button);
                        optionsText.setPosition(optionsButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= quitButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= quitButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= quitButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= quitButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        quitButtonImage.setTexture(buttonPressed);
                        quitText.setPosition(quitButtonImage.getPosition().x+2, quitButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        quitButtonImage.setTexture(button);
                        quitText.setPosition(quitButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= vaisseauSprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseauSprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseauSprite.getPosition().y-TAILLE_VAISSEAU/2 && event.mouseMove.y <= vaisseauSprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1Dark);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2Dark);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3Dark);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4Dark);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5Dark);
                            break;
                        }
                    }
                    else
                    {
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            break;
                        }
                    }

                    if(event.mouseMove.x >= trophySprite.getPosition().x-TAILLE_BOUTONCARRE/2 && event.mouseMove.x <= trophySprite.getPosition().x+TAILLE_BOUTONCARRE/2 && event.mouseMove.y >= trophySprite.getPosition().y-TAILLE_BOUTONCARRE/2 && event.mouseMove.y <= trophySprite.getPosition().y+TAILLE_BOUTONCARRE/2)
                        trophySprite.setTexture(trophyButtonPressed);
                    else
                        trophySprite.setTexture(trophyButton);
                }
                else if(menu == 2) // SI ON EST DANS LE MENU OPTIONS
                {
                    if(event.mouseMove.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        backButtonImage.setTexture(buttonPressed);
                        backText.setPosition(backButtonImage.getPosition().x+2, backButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        backButtonImage.setTexture(button);
                        backText.setPosition(backButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= backSprite.getPosition().x-TAILLE_PETITBOUTON/2 && event.mouseMove.x <= backSprite.getPosition().x+TAILLE_PETITBOUTON/2 && event.mouseMove.y >= backSprite.getPosition().y-TAILLE_PETITBOUTON/2 && event.mouseMove.y <= backSprite.getPosition().y+TAILLE_PETITBOUTON/2)
                        backSprite.setTexture(backPressed);
                    else
                        backSprite.setTexture(back);

                    if(event.mouseMove.x >= forwardSprite.getPosition().x-TAILLE_PETITBOUTON/2 && event.mouseMove.x <= forwardSprite.getPosition().x+TAILLE_PETITBOUTON/2 && event.mouseMove.y >= forwardSprite.getPosition().y-TAILLE_PETITBOUTON/2 && event.mouseMove.y <= forwardSprite.getPosition().y+TAILLE_PETITBOUTON/2)
                        forwardSprite.setTexture(forwardPressed);
                    else
                        forwardSprite.setTexture(forward);
                }
                else if(menu == 3) // SI ON EST DANS LE MENU CHOIX VAISSEAU
                {
                    if(event.mouseMove.x >= vaisseau1Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau1Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau1Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau1Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[0]);
                        nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x-nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y+TAILLE_VAISSEAU/2);
                        vaisseau1Sprite.setTexture(vaisseau1);
                    }
                    else
                        vaisseau1Sprite.setTexture(vaisseau1Dark);

                    if(event.mouseMove.x >= vaisseau2Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau2Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau2Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau2Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[1]);
                        nomVaisseauText.setPosition(vaisseau2Sprite.getPosition().x-nomVaisseauText.getLocalBounds().width/2, vaisseau2Sprite.getPosition().y+TAILLE_VAISSEAU/2);
                        vaisseau2Sprite.setTexture(vaisseau2);
                    }
                    else
                        vaisseau2Sprite.setTexture(vaisseau2Dark);

                    if(event.mouseMove.x >= vaisseau3Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau3Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau3Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau3Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[2]);
                        nomVaisseauText.setPosition(vaisseau3Sprite.getPosition().x-nomVaisseauText.getLocalBounds().width/2, vaisseau3Sprite.getPosition().y+TAILLE_VAISSEAU/2);
                        vaisseau3Sprite.setTexture(vaisseau3);
                    }
                    else
                        vaisseau3Sprite.setTexture(vaisseau3Dark);

                    if(event.mouseMove.x >= vaisseau4Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau4Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau4Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau4Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[3]);
                        nomVaisseauText.setPosition(vaisseau4Sprite.getPosition().x-nomVaisseauText.getLocalBounds().width/2, vaisseau4Sprite.getPosition().y+TAILLE_VAISSEAU/2);
                        vaisseau4Sprite.setTexture(vaisseau4);
                    }
                    else
                        vaisseau4Sprite.setTexture(vaisseau4Dark);

                    if(event.mouseMove.x >= vaisseau5Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau5Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau5Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau5Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[4]);
                        nomVaisseauText.setPosition(vaisseau5Sprite.getPosition().x-nomVaisseauText.getLocalBounds().width/2, vaisseau5Sprite.getPosition().y+TAILLE_VAISSEAU/2);
                        vaisseau5Sprite.setTexture(vaisseau5);
                    }
                    else
                        vaisseau5Sprite.setTexture(vaisseau5Dark);

                    switch(joueur.vaisseau.id)
                    {
                    case 1:
                        vaisseau1Sprite.setTexture(vaisseau1Choice);
                        break;
                    case 2:
                        vaisseau2Sprite.setTexture(vaisseau2Choice);
                        break;
                    case 3:
                        vaisseau3Sprite.setTexture(vaisseau3Choice);
                        break;
                    case 4:
                        vaisseau4Sprite.setTexture(vaisseau4Choice);
                        break;
                    case 5:
                        vaisseau5Sprite.setTexture(vaisseau5Choice);
                        break;
                    }

                    if(event.mouseMove.x >= validationButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= validationButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= validationButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= validationButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        validationButtonImage.setTexture(buttonPressed);
                        validationText.setPosition(validationButtonImage.getPosition().x+2, validationButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        validationButtonImage.setTexture(button);
                        validationText.setPosition(validationButtonImage.getPosition());
                    }
                }
                else if(menu == 4) // SI ON EST DANS LE MENU SCORES
                {
                    if(event.mouseMove.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        backButtonImage.setTexture(buttonPressed);
                        backText.setPosition(backButtonImage.getPosition().x+2, backButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        backButtonImage.setTexture(button);
                        backText.setPosition(backButtonImage.getPosition());
                    }
                }
                else if(menu == 5) // SI ON EST DANS LE MENU CHOIX DIFFICULTE
                {
                    if(event.mouseMove.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        backButtonImage.setTexture(buttonPressed);
                        backText.setPosition(backButtonImage.getPosition().x+2, backButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        backButtonImage.setTexture(button);
                        backText.setPosition(backButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= difficulte1Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseMove.x <= difficulte1Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseMove.y >= difficulte1Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseMove.y <= difficulte1Sprite.getPosition().y+TAILLE_ROND/2)
                        difficulte1Sprite.setTexture(roundPressed);
                    else
                        difficulte1Sprite.setTexture(round);

                    if(event.mouseMove.x >= difficulte2Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseMove.x <= difficulte2Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseMove.y >= difficulte2Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseMove.y <= difficulte2Sprite.getPosition().y+TAILLE_ROND/2)
                        difficulte2Sprite.setTexture(roundPressed);
                    else
                        difficulte2Sprite.setTexture(round);

                    if(event.mouseMove.x >= difficulte3Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseMove.x <= difficulte3Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseMove.y >= difficulte3Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseMove.y <= difficulte3Sprite.getPosition().y+TAILLE_ROND/2)
                        difficulte3Sprite.setTexture(roundPressed);
                    else
                        difficulte3Sprite.setTexture(round);
                }
                else if(menu == 99 && gameover == 1)
                {
                    if(event.mouseMove.x >= returnToMenuButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= returnToMenuButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= returnToMenuButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= returnToMenuButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        returnToMenuButtonImage.setTexture(buttonPressed);
                        playAgainText.setPosition(returnToMenuButtonImage.getPosition().x+2, returnToMenuButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        returnToMenuButtonImage.setTexture(button);
                        playAgainText.setPosition(returnToMenuButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= playAgainButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= playAgainButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= playAgainButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= playAgainButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        playAgainButtonImage.setTexture(buttonPressed);
                        playAgainText.setPosition(playAgainButtonImage.getPosition().x+2, playAgainButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        playAgainButtonImage.setTexture(button);
                        playAgainText.setPosition(playAgainButtonImage.getPosition());
                    }
                }
            }
            if (event.mouseButton.button == Mouse::isButtonPressed(Mouse::Left))
            {
                if(menu == 1) // SI ON EST DANS LE MENU PRINCIPAL
                {
                    if(event.mouseButton.x >= startButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= startButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= startButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= startButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 5;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= optionsButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= optionsButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= optionsButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= optionsButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 2;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= vaisseauSprite.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= vaisseauSprite.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= vaisseauSprite.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= vaisseauSprite.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 3;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= trophySprite.getPosition().x-TAILLE_BOUTONCARRE && event.mouseButton.x <= trophySprite.getPosition().x+TAILLE_BOUTONCARRE && event.mouseButton.y >= trophySprite.getPosition().y-TAILLE_BOUTONCARRE && event.mouseButton.y <= trophySprite.getPosition().y+TAILLE_BOUTONCARRE)
                    {
                        menu = 4;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= quitButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= quitButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= quitButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= quitButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                        window.close();
                }
                else if (menu == 2) // SI ON EST DANS LE MENU OPTIONS
                {
                    if(event.mouseButton.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x - nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y);
                    }

                    if(event.mouseButton.x >= backSprite.getPosition().x-TAILLE_PETITBOUTON && event.mouseButton.x <= backSprite.getPosition().x+TAILLE_PETITBOUTON && event.mouseButton.y >= backSprite.getPosition().y-TAILLE_PETITBOUTON && event.mouseButton.y <= backSprite.getPosition().y+TAILLE_PETITBOUTON)
                    {
                        if(volume != 0)
                            volume = volume-25;
                        sound.setVolume(volume);
                        switch(volume)
                        {
                        case 0:
                            barSprite.setTexture(bar);
                            break;
                        case 25:
                            barSprite.setTexture(bar25);
                            break;
                        case 50:
                            barSprite.setTexture(bar50);
                            break;
                        case 75:
                            barSprite.setTexture(bar75);
                            break;
                        case 100:
                            barSprite.setTexture(bar100);
                            break;
                        }
                    }

                    if(event.mouseButton.x >= forwardSprite.getPosition().x-TAILLE_PETITBOUTON && event.mouseButton.x <= forwardSprite.getPosition().x+TAILLE_PETITBOUTON && event.mouseButton.y >= forwardSprite.getPosition().y-TAILLE_PETITBOUTON && event.mouseButton.y <= forwardSprite.getPosition().y+TAILLE_PETITBOUTON)
                    {
                        if(volume != 100)
                            volume = volume+25;
                        sound.setVolume(volume);
                        switch(volume)
                        {
                        case 0:
                            barSprite.setTexture(bar);
                            break;
                        case 25:
                            barSprite.setTexture(bar25);
                            break;
                        case 50:
                            barSprite.setTexture(bar50);
                            break;
                        case 75:
                            barSprite.setTexture(bar75);
                            break;
                        case 100:
                            barSprite.setTexture(bar100);
                            break;
                        }
                    }
                }
                else if(menu == 3) // SI ON EST DANS LE MENU CHOIX VAISSEAU
                {
                    vaisseau1Sprite.setTexture(vaisseau1Dark);
                    vaisseau2Sprite.setTexture(vaisseau2Dark);
                    vaisseau3Sprite.setTexture(vaisseau3Dark);
                    vaisseau4Sprite.setTexture(vaisseau4Dark);
                    vaisseau5Sprite.setTexture(vaisseau5Dark);

                    if(event.mouseButton.x >= vaisseau1Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau1Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau1Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau1Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 1;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[0]);
                        vaisseau1Sprite.setTexture(vaisseau1Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau2Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau2Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau2Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau2Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 2;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[1]);
                        vaisseau2Sprite.setTexture(vaisseau2Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau3Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau3Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau3Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau3Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 3;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[2]);
                        vaisseau3Sprite.setTexture(vaisseau3Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau4Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau4Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau4Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau4Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 4;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[3]);
                        vaisseau4Sprite.setTexture(vaisseau4Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau5Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau5Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau5Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau5Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 5;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[4]);
                        vaisseau5Sprite.setTexture(vaisseau5Choice);
                    }
                    else if(event.mouseButton.x >= validationButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= validationButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= validationButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= validationButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x - nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y);
                    }
                }
                else if (menu == 4) // SI ON EST DANS LE MENU SCORES
                {
                    if(event.mouseButton.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x - nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y);
                    }
                }
                else if (menu == 5) // SI ON EST DANS LE MENU CHOIX DIFFICULTE
                {
                    if(event.mouseButton.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x - nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y);
                    }
                    else if(event.mouseButton.x >= difficulte1Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseButton.x <= difficulte1Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseButton.y >= difficulte1Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseButton.y <= difficulte1Sprite.getPosition().y+TAILLE_ROND/2)
                    {
                        difficulte = 1;
                        menu = 99;
                    }
                    else if(event.mouseButton.x >= difficulte2Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseButton.x <= difficulte2Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseButton.y >= difficulte2Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseButton.y <= difficulte2Sprite.getPosition().y+TAILLE_ROND/2)
                    {
                        difficulte = 2;
                        menu = 99;
                    }
                    else if(event.mouseButton.x >= difficulte3Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseButton.x <= difficulte3Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseButton.y >= difficulte3Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseButton.y <= difficulte3Sprite.getPosition().y+TAILLE_ROND/2)
                    {
                        difficulte = 3;
                        menu = 99;
                    }
                }
                else if(menu == 99 && gameover == 1)
                {
                    if(event.mouseButton.x >= returnToMenuButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= returnToMenuButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= returnToMenuButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= returnToMenuButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        gameover = 0;
                        menu = 1;
                    }
                    else if(event.mouseButton.x >= playAgainButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= playAgainButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= playAgainButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= playAgainButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        gameover = 0;
                        menu = 5;
                    }
                }
            }
        }

        window.clear();

        // Sprites drawing

        window.draw(backgroundSprite);
        window.draw(logoSprite);

        if(menu == 1) // SI ON EST DANS LE MENU PRINCIPAL
        {
            window.draw(startButtonImage);
            window.draw(optionsButtonImage);
            window.draw(quitButtonImage);
            window.draw(startText);
            window.draw(optionsText);
            window.draw(quitText);
            window.draw(vaisseauSprite);
            window.draw(nomVaisseauText);
            window.draw(trophySprite);
        }
        else if(menu == 2) // SI ON EST DANS LE MENU OPTIONS
        {
            window.draw(backButtonImage);
            window.draw(backText);
            window.draw(barSprite);
            window.draw(volumeChoiceText);

            // Mettre � jour le volume et le centrer
            char str[3];
            sprintf(str, "%i", volume);

            volumeNumberText.setString(str);
            FloatRect bounds = volumeNumberText.getLocalBounds();
            volumeNumberText.setOrigin(bounds.width/2, bounds.height);

            window.draw(volumeNumberText);
            window.draw(backSprite);
            window.draw(forwardSprite);

        }
        else if(menu == 3) // SI ON EST DANS LE MENU CHOIX VAISSEAU
        {
            window.draw(vaisseau1Sprite);
            window.draw(vaisseau2Sprite);
            window.draw(vaisseau3Sprite);
            window.draw(vaisseau4Sprite);
            window.draw(vaisseau5Sprite);
            window.draw(choixVaisseauText);
            window.draw(validationButtonImage);
            window.draw(validationText);
            window.draw(nomVaisseauText);

            switch(joueur.vaisseau.id)
            {
            case 1:
                vaisseau1Sprite.setTexture(vaisseau1Choice);
                break;
            case 2:
                vaisseau2Sprite.setTexture(vaisseau2Choice);
                break;
            case 3:
                vaisseau3Sprite.setTexture(vaisseau3Choice);
                break;
            case 4:
                vaisseau4Sprite.setTexture(vaisseau4Choice);
                break;
            case 5:
                vaisseau5Sprite.setTexture(vaisseau5Choice);
                break;
            }
        }

        else if(menu == 4) // SI ON EST DANS LE MENU SCORES
        {
            window.draw(backButtonImage);
            window.draw(backText);
            window.draw(scoresText);

            // Mettre � jour le score 1 et le centrer
            char str[20];
            sprintf(str, "[1] %i", scores[0]);

            score1Text.setString(str);
            FloatRect bounds = score1Text.getLocalBounds();
            score1Text.setOrigin(bounds.width/2, bounds.height);

            window.draw(score1Text);

            // Mettre � jour le score 2 et le centrer
            char str2[20];
            sprintf(str2, "[2] %i", scores[1]);

            score2Text.setString(str2);
            FloatRect bounds2 = score2Text.getLocalBounds();
            score2Text.setOrigin(bounds2.width/2, bounds2.height);

            window.draw(score2Text);

            // Mettre � jour le score 3 et le centrer
            char str3[20];
            sprintf(str3, "[3] %i", scores[2]);

            score3Text.setString(str3);
            FloatRect bounds3 = score3Text.getLocalBounds();
            score3Text.setOrigin(bounds3.width/2, bounds3.height);

            window.draw(score3Text);
        }
        else if (menu == 5) // SI ON EST DANS LE MENU CHOIX DIFFICULTE
        {
            window.draw(difficulteChoiceText);
            window.draw(backButtonImage);
            window.draw(backText);
            window.draw(difficulte1Sprite);
            window.draw(difficulte2Sprite);
            window.draw(difficulte3Sprite);
            window.draw(difficulte1Text);
            window.draw(difficulte2Text);
            window.draw(difficulte3Text);
        }
        else if (menu == 99) // SI ON EST DANS LE JEU
        {
            // Mettre � jour la diffult� et la centrer
            char str[12];
            sprintf(str, "%i", difficulte);

            difficulteNumberText.setString(str);
            FloatRect bounds = difficulteNumberText.getLocalBounds();
            difficulteNumberText.setOrigin(bounds.width/2, bounds.height);

            // Mettre � jour le score et le centrer
            char str2[12];
            sprintf(str2, "%i", score);

            scoreNumberText.setString(str2);
            FloatRect bounds2 = scoreNumberText.getLocalBounds();
            scoreNumberText.setOrigin(bounds2.width/2, bounds2.height);

            // Draw the sprite
            window.draw(inGamebackgroundSprite);
            window.draw(titleText);
            window.draw(difficulteText);
            window.draw(levelRoundSprite);
            window.draw(difficulteNumberText);
            window.draw(scoreText);
            window.draw(scoreBoxSprite);
            window.draw(scoreNumberText);
            window.draw(viesText);

            switch(vies)
            {
            case 0:
                window.draw(starEmpty1);
                window.draw(starEmpty2);
                window.draw(starEmpty3);
                break;
            case 1:
                window.draw(star1);
                window.draw(starEmpty2);
                window.draw(starEmpty3);
                break;
            case 2:
                window.draw(star1);
                window.draw(star2);
                window.draw(starEmpty3);
                break;
            case 3:
                window.draw(star1);
                window.draw(star2);
                window.draw(star3);
                break;
            }

            window.draw(bonusText);
            window.draw(bonus1RoundSprite);
            window.draw(bonus2RoundSprite);
            window.draw(bonus3RoundSprite);

            if(bonus[0] == 1)
                window.draw(bonus1Sprite);
            else
                window.draw(bonus1SpriteEmpty);

            if(bonus[1] == 1)
                window.draw(bonus2Sprite);
            else
                window.draw(bonus2SpriteEmpty);

            if(bonus[2] == 1)
                window.draw(bonus3Sprite);
            else
                window.draw(bonus3SpriteEmpty);

            if(gameover == 1)
            {
                window.draw(gameOverText);

                // Mettre � jour le score et le centrer
                char str[12];
                sprintf(str, "%i", score);

                gameOverScoreText.setString(str);
                FloatRect bounds = gameOverScoreText.getLocalBounds();
                gameOverScoreText.setOrigin(bounds.width/2, bounds.height);

                window.draw(gameOverScoreText);
                window.draw(returnToMenuButtonImage);
                window.draw(returnToMenuText);
                window.draw(playAgainButtonImage);
                window.draw(playAgainText);
            }
        }
        window.display();
    }

    return 0;
}
