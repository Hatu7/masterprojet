#ifndef BONUS_HPP_INCLUDED
#define BONUS_HPP_INCLUDED
#include "autre.hpp"
#include "projectiles.hpp"
#define TAILLE_VAISSEAU 200
using namespace sf;

void creationBonus (bool *bonusPresent, int *bonusType, Clock *apparitionBonus, Sprite *Bonus, Texture *Bonus1, Texture *Bonus2, Texture *Bonus3);
void bonusRalenti(bool * active, sf::Clock * clockBonus, int * vitesseVaisseau,int bonus[],bool autre);
void bonusBombe(Projectile simple[], int nbProjectile, int bonus[], Point posVaisseau);
void bonusVitesse(bool *active,sf::Clock *clockVitesse, int * vitesseVaisseau,int bonus[],bool autre);
void bonusRecup (bool *bonusPresent, int bonusType, int bonus[], Sprite Vaisseau, Sprite Bonus);


#endif // BONUS_HPP_INCLUDED
