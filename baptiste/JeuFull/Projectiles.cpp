#include <SFML/Graphics.hpp>
#include "projectiles.hpp"
#include "autre.hpp"
#include "mouvement.hpp"
using namespace sf;

void creationProjectileSimple(int nb, Projectile simple[])
{
    simple[nb].position.x = alea(TAILLE_PROJECTILE_SIMPLE, TAILLE_FENETRE_X - TAILLE_MENU_SCORE - TAILLE_PROJECTILE_SIMPLE);
    simple[nb].position.y = alea(TAILLE_PROJECTILE_SIMPLE, TAILLE_FENETRE_Y - TAILLE_PROJECTILE_SIMPLE);
    simple[nb].couleur = alea(1, 2);
    simple[nb].vitesseX = alea(1,4);
    if (alea(1,2) == 2)
        simple[nb].vitesseX = -simple[nb].vitesseX;
    simple[nb].vitesseY = alea(1,4);
    if (alea(1,2) == 2)
        simple[nb].vitesseY = -simple[nb].vitesseY;
}

void deplacementProjectileSimple (int nb, Projectile simple[], bool bonus1)
{
    float slow = 1;
    if (bonus1 == true)
        slow = 0.5;
    simple[nb].position.x += simple[nb].vitesseX * slow;
    simple[nb].position.y += simple[nb].vitesseY * slow;
    if (simple[nb].position.x <= 0 || simple[nb].position.x >= TAILLE_FENETRE_X - TAILLE_MENU_SCORE - TAILLE_PROJECTILE_SIMPLE)
    {
        simple[nb].vitesseX = -simple[nb].vitesseX;
    }
    if (simple[nb].position.y <= 0 || simple[nb].position.y >= TAILLE_FENETRE_Y - TAILLE_PROJECTILE_SIMPLE)
    {
        simple[nb].vitesseY = -simple[nb].vitesseY;
    }
}

void affichageProjectileSimple (int i, int vies, bool bonus1, Projectile simple[], Clock clock, Sprite *projectileSimple, Texture *imageProjectileSimpleRouge, Texture *imageProjectileSimpleBleu)
{
    if (clock.getElapsedTime().asSeconds() < (i+1) * ECART_SIMPLE + 1.5)
        (*projectileSimple).setColor(Color(255,255,255,128));
    else
    {
        if (vies != 0)
            deplacementProjectileSimple(i, simple, bonus1);
        (*projectileSimple).setColor(Color(255,255,255,255));
    }
    if (simple[i].couleur == 1)
        (*projectileSimple).setTexture(*imageProjectileSimpleRouge);
    else
        (*projectileSimple).setTexture(*imageProjectileSimpleBleu);
    (*projectileSimple).setPosition(simple[i].position.x,simple[i].position.y);
}

void checkCollision(int nbProjectile, int *vies, Projectile simple[], Point posVaisseau, Clock clock, Clock *invincibilite)
{
    for (int i=0; i<nbProjectile; i++)
    {
        if (simple[i].position.x<=(posVaisseau.x+TAILLE_VAISSEAU/4 - 10) && simple[i].position.x>=(posVaisseau.x-TAILLE_VAISSEAU/4 + 10) &&
            simple[i].position.y<=(posVaisseau.y+TAILLE_VAISSEAU/4 - 10) && simple[i].position.y>=(posVaisseau.y-TAILLE_VAISSEAU/4 + 10) &&
            clock.getElapsedTime().asSeconds() > (i+1) * ECART_SIMPLE + 1.5 && (*invincibilite).getElapsedTime().asSeconds() >= 2)
        {
            *vies -= 1;
            (*invincibilite).restart();
        }
    }
}
