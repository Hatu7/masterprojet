#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string.h>
#include <iostream>

#define LARGEUR_FENETRE 1200
#define HAUTEUR_FENETRE 800
#define TAILLE_TEXTE_TITREBARRE 40
#define TAILLE_TEXTE_TITRELONGBARRE 30
#define TAILLE_TEXTE_SMALLTEXTEBARRE 20

using namespace sf;
using namespace std;

int difficulte = 3;
int score = 54140;
int vies = 2;
int bonus[3] = {0,1,0};

int main()
{
    // Create the main window
    RenderWindow window(VideoMode(1200, 800), "Space Escape");

    Texture background;
    background.loadFromFile("inGameBackground.png");
    Sprite backgroundSprite(background);

    Font font;
    font.loadFromFile("SPACE.ttf");

    // BARRE A DROITE //
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text titleText;
    titleText.setFont(font);
    titleText.setString("Infos");
    titleText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    titleText.setFillColor(Color::White);
    titleText.setOrigin(titleText.getLocalBounds().width/2, titleText.getLocalBounds().height);
    titleText.setPosition(1050,52);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text difficulteText;
    difficulteText.setFont(font);
    difficulteText.setString("Difficulte");
    difficulteText.setCharacterSize(TAILLE_TEXTE_TITRELONGBARRE);
    difficulteText.setFillColor(Color::White);
    difficulteText.setOrigin(difficulteText.getLocalBounds().width/2, difficulteText.getLocalBounds().height);
    difficulteText.setPosition(1050,130);

    Texture round01;
    round01.loadFromFile("round01.png");

    Sprite levelRoundSprite;
    levelRoundSprite.setTexture(round01);
    levelRoundSprite.setOrigin(levelRoundSprite.getLocalBounds().width/2, levelRoundSprite.getLocalBounds().height);
    levelRoundSprite.setPosition(1050,235);

    Text difficulteNumberText;
    difficulteNumberText.setFont(font);
    difficulteNumberText.setPosition(1047,200);
    difficulteNumberText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    difficulteNumberText.setFillColor(Color::White);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text scoreText;
    scoreText.setFont(font);
    scoreText.setString("Score");
    scoreText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    scoreText.setFillColor(Color::White);
    scoreText.setOrigin(scoreText.getLocalBounds().width/2, scoreText.getLocalBounds().height);
    scoreText.setPosition(1050,290);

    Texture box01;
    box01.loadFromFile("box01.png");

    Sprite scoreBoxSprite;
    scoreBoxSprite.setTexture(box01);
    scoreBoxSprite.setOrigin(scoreBoxSprite.getLocalBounds().width/2, scoreBoxSprite.getLocalBounds().height);
    scoreBoxSprite.setPosition(1050,360);

    Text scoreNumberText;
    scoreNumberText.setFont(font);
    scoreNumberText.setPosition(1050,342);
    scoreNumberText.setCharacterSize(TAILLE_TEXTE_SMALLTEXTEBARRE);
    scoreNumberText.setFillColor(Color::White);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text viesText;
    viesText.setFont(font);
    viesText.setString("Vies");
    viesText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    viesText.setFillColor(Color::White);
    viesText.setOrigin(viesText.getLocalBounds().width/2, viesText.getLocalBounds().height);
    viesText.setPosition(1050,410);

    Texture starEmpty;
    starEmpty.loadFromFile("starEmpty.png");
    Texture star;
    star.loadFromFile("star.png");

    Sprite starEmpty1;
    starEmpty1.setTexture(starEmpty);
    starEmpty1.setOrigin(starEmpty1.getLocalBounds().width/2, starEmpty1.getLocalBounds().height);
    starEmpty1.setPosition(965,515);

    Sprite starEmpty2;
    starEmpty2.setTexture(starEmpty);
    starEmpty2.setOrigin(starEmpty2.getLocalBounds().width/2, starEmpty2.getLocalBounds().height);
    starEmpty2.setPosition(1050,515);

    Sprite starEmpty3;
    starEmpty3.setTexture(starEmpty);
    starEmpty3.setOrigin(starEmpty3.getLocalBounds().width/2, starEmpty3.getLocalBounds().height);
    starEmpty3.setPosition(1135,515);

    Sprite star1;
    star1.setTexture(star);
    star1.setOrigin(star1.getLocalBounds().width/2, star1.getLocalBounds().height);
    star1.setPosition(965,515);

    Sprite star2;
    star2.setTexture(star);
    star2.setOrigin(star2.getLocalBounds().width/2, star2.getLocalBounds().height);
    star2.setPosition(1050,515);

    Sprite star3;
    star3.setTexture(star);
    star3.setOrigin(star3.getLocalBounds().width/2, star3.getLocalBounds().height);
    star3.setPosition(1135,515);
    //////////////////////////////////////////////////////////////////////////////////////////////
    Text bonusText;
    bonusText.setFont(font);
    bonusText.setString("Bonus");
    bonusText.setCharacterSize(TAILLE_TEXTE_TITREBARRE);
    bonusText.setFillColor(Color::White);
    bonusText.setOrigin(bonusText.getLocalBounds().width/2, bonusText.getLocalBounds().height);
    bonusText.setPosition(1050,565);

    Texture round02;
    round02.loadFromFile("round02.png");

    Sprite bonus1RoundSprite;
    bonus1RoundSprite.setTexture(round02);
    bonus1RoundSprite.setOrigin(bonus1RoundSprite.getLocalBounds().width/2, bonus1RoundSprite.getLocalBounds().height);
    bonus1RoundSprite.setPosition(965,680);

    Sprite bonus2RoundSprite;
    bonus2RoundSprite.setTexture(round02);
    bonus2RoundSprite.setOrigin(bonus2RoundSprite.getLocalBounds().width/2, bonus2RoundSprite.getLocalBounds().height);
    bonus2RoundSprite.setPosition(1050,680);

    Sprite bonus3RoundSprite;
    bonus3RoundSprite.setTexture(round02);
    bonus3RoundSprite.setOrigin(bonus3RoundSprite.getLocalBounds().width/2, bonus3RoundSprite.getLocalBounds().height);
    bonus3RoundSprite.setPosition(1135,680);

    Texture bonus1Time;
    bonus1Time.loadFromFile("bonus1Time.png");
    Texture bonus2Bomb;
    bonus2Bomb.loadFromFile("bonus2Bomb.png");
    Texture bonus3Speed;
    bonus3Speed.loadFromFile("bonus3Speed.png");
    Texture bonus1TimeEmpty;
    bonus1TimeEmpty.loadFromFile("bonus1TimeEmpty.png");
    Texture bonus2BombEmpty;
    bonus2BombEmpty.loadFromFile("bonus2BombEmpty.png");
    Texture bonus3SpeedEmpty;
    bonus3SpeedEmpty.loadFromFile("bonus3SpeedEmpty.png");

    Sprite bonus1Sprite;
    bonus1Sprite.setTexture(bonus1Time);
    bonus1Sprite.setOrigin(bonus1Sprite.getLocalBounds().width/2, bonus1Sprite.getLocalBounds().height);
    bonus1Sprite.setScale(Vector2f(0.8, 0.8));
    bonus1Sprite.setPosition(965,662);

    Sprite bonus2Sprite;
    bonus2Sprite.setTexture(bonus2Bomb);
    bonus2Sprite.setOrigin(bonus2Sprite.getLocalBounds().width/2, bonus2Sprite.getLocalBounds().height);
    bonus2Sprite.setScale(Vector2f(0.8, 0.8));
    bonus2Sprite.setPosition(1050,662);

    Sprite bonus3Sprite;
    bonus3Sprite.setTexture(bonus3Speed);
    bonus3Sprite.setOrigin(bonus3Sprite.getLocalBounds().width/2, bonus3Sprite.getLocalBounds().height);
    bonus3Sprite.setScale(Vector2f(0.8, 0.8));
    bonus3Sprite.setPosition(1135,659);

    Sprite bonus1SpriteEmpty;
    bonus1SpriteEmpty.setTexture(bonus1TimeEmpty);
    bonus1SpriteEmpty.setOrigin(bonus1SpriteEmpty.getLocalBounds().width/2, bonus1SpriteEmpty.getLocalBounds().height);
    bonus1SpriteEmpty.setScale(Vector2f(0.8, 0.8));
    bonus1SpriteEmpty.setPosition(965,662);

    Sprite bonus2SpriteEmpty;
    bonus2SpriteEmpty.setTexture(bonus2BombEmpty);
    bonus2SpriteEmpty.setOrigin(bonus2SpriteEmpty.getLocalBounds().width/2, bonus2SpriteEmpty.getLocalBounds().height);
    bonus2SpriteEmpty.setScale(Vector2f(0.8, 0.8));
    bonus2SpriteEmpty.setPosition(1050,662);

    Sprite bonus3SpriteEmpty;
    bonus3SpriteEmpty.setTexture(bonus3SpeedEmpty);
    bonus3SpriteEmpty.setOrigin(bonus3SpriteEmpty.getLocalBounds().width/2, bonus3SpriteEmpty.getLocalBounds().height);
    bonus3SpriteEmpty.setScale(Vector2f(0.8, 0.8));
    bonus3SpriteEmpty.setPosition(1135,659);

    //////////////////////////////////////////////////////////////////////////////////////////////

    // Start the game loop
    while (window.isOpen())
    {
        // Mettre � jour la diffult� et la centrer
        char str[12];
        sprintf(str, "%i", difficulte);

        difficulteNumberText.setString(str);
        FloatRect bounds = difficulteNumberText.getLocalBounds();
        difficulteNumberText.setOrigin(bounds.width/2, bounds.height);

        // Mettre � jour le score et le centrer
        char str2[12];
        sprintf(str2, "%i", score);

        scoreNumberText.setString(str2);
        FloatRect bounds2 = scoreNumberText.getLocalBounds();
        scoreNumberText.setOrigin(bounds2.width/2, bounds2.height);

        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // Clear screen
        window.clear();

        // Draw the sprite
        window.draw(backgroundSprite);
        window.draw(titleText);
        window.draw(difficulteText);
        window.draw(levelRoundSprite);
        window.draw(difficulteNumberText);
        window.draw(scoreText);
        window.draw(scoreBoxSprite);
        window.draw(scoreNumberText);
        window.draw(viesText);

        switch(vies)
        {
        case 0:
            window.draw(starEmpty1);
            window.draw(starEmpty2);
            window.draw(starEmpty3);
            break;
        case 1:
            window.draw(star1);
            window.draw(starEmpty2);
            window.draw(starEmpty3);
            break;
        case 2:
            window.draw(star1);
            window.draw(star2);
            window.draw(starEmpty3);
            break;
        case 3:
            window.draw(star1);
            window.draw(star2);
            window.draw(star3);
            break;
        }

        window.draw(bonusText);
        window.draw(bonus1RoundSprite);
        window.draw(bonus2RoundSprite);
        window.draw(bonus3RoundSprite);

        if(bonus[0] == 1)
            window.draw(bonus1Sprite);
        else
            window.draw(bonus1SpriteEmpty);

        if(bonus[1] == 1)
            window.draw(bonus2Sprite);
        else
            window.draw(bonus2SpriteEmpty);

        if(bonus[2] == 1)
            window.draw(bonus3Sprite);
        else
            window.draw(bonus3SpriteEmpty);

        // Update the window
        window.display();
    }

    return EXIT_SUCCESS;
}
