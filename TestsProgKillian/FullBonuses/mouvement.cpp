#include <SFML/Graphics.hpp>
#include "mouvement.hpp"
using namespace sf;

void mouvement(Point *pos, int speed)
{
    if (Keyboard::isKeyPressed(Keyboard::LShift))
        speed=speed*0.7;
    if(Keyboard::isKeyPressed(Keyboard::S) && pos->y <= TAILLE_FENETRE_Y - TAILLE_VAISSEAU/4 - 30)
        pos->y +=speed;
    if(Keyboard::isKeyPressed(Keyboard::Z) && pos->y >= 0 + 30)
        pos->y -= speed;
    if(Keyboard::isKeyPressed(Keyboard::Q) && pos->x >= 0 + 30)
        pos->x -= speed;
    if(Keyboard::isKeyPressed(Keyboard::D) && pos->x <= TAILLE_FENETRE_X - TAILLE_MENU_SCORE - TAILLE_VAISSEAU/4 - 30)
        pos->x += speed;
}
