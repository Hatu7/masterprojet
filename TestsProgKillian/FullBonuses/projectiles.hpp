#ifndef PROJECTILES_HPP_INCLUDED
#define PROJECTILES_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include "autre.hpp"
#define TAILLE_PROJECTILE_SIMPLE 16
#define ECART_SIMPLE 3


typedef struct {
    Point position;
    int taille;
    int couleur;
    float vitesseX;
    float vitesseY;
}Projectile;

void creationProjectileSimple(int nb, Projectile simple[]);
void deplacementProjectileSimple (int nb, Projectile simple[], bool bonus1);
void affichageProjectileSimple (int i, int vies, bool bonus1, Projectile simple[], sf::Clock clock, sf::Sprite *projectileSimple, sf::Texture *imageProjectileSimpleRouge, sf::Texture *imageProjectileSimpleBleu);
void checkCollision(int nbProjectile, int *vies, Projectile simple[], Point posVaisseau, sf::Clock clock, sf::Clock *invincibilite);

#endif // PROJECTILES_H_INCLUDED
