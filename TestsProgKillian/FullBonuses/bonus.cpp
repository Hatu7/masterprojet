#include <SFML/Graphics.hpp>
#include "bonus.hpp"
using namespace sf;

void bonusRalenti(bool * active,sf::Clock * clockBonus, int * vitesseVaisseau,int bonus[],bool autre) //A compléter avec le ralentissement de vitesse des projectiles, comme la vitesse est un int j'ai mis 5 et 2 au lieu de faire un vitesse/2
{
    if (Keyboard::isKeyPressed(Keyboard::I))
        bonus[0]=1;
    if((Keyboard::isKeyPressed(Keyboard::Space))&&bonus[0]==1)
    {
        *active=true;
        bonus[0]=0;
        clockBonus->restart();
    }
    if (clockBonus->getElapsedTime().asSeconds() <= 5 && *active==true)
        {
            *vitesseVaisseau=2;
            printf("ralenti");
        }
    else if (autre==false)
    {
        *vitesseVaisseau=5;
        *active=false;
    }
}
void bonusVitesse(bool *active,sf::Clock *clockVitesse, int * vitesseVaisseau,int bonus[],bool autre)
{
    if (Keyboard::isKeyPressed(Keyboard::O))
        bonus[2]=1;
    if (Keyboard::isKeyPressed(Keyboard::Space)&&bonus[2]==1)
    {
        *active=true;
        bonus[2]=0;
        clockVitesse->restart();
    }
    if (clockVitesse->getElapsedTime().asSeconds() <=5 && *active==true)
    {
        *vitesseVaisseau=8;
        printf("active");
    }
    else if (autre==false)
    {
        *vitesseVaisseau=5;
        *active=false;
    }
}
