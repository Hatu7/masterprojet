#ifndef AUTRE_HPP_INCLUDED
#define AUTRE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#define TAILLE_FENETRE_X 1200
#define TAILLE_FENETRE_Y 800
#define TAILLE_MENU_SCORE 300

typedef struct {
    float x;
    float y;
}Point;

int alea(int mini, int maxi);

#endif // AUTRE_HPP_INCLUDED
