#include <SFML/Graphics.hpp>
#define SPEED 8
#define SIZE 30
using namespace sf;
typedef struct
{
    int x;
    int y;
} Position;

int main()
{
    Position posPerso;
    int speed=SPEED;
    // Create the main window
    RenderWindow mouvement(VideoMode(800, 600), "Tests Mouvement 1");
    mouvement.setFramerateLimit(60);

    // Load a sprite to display
    Texture vaisseau1;
    if (!vaisseau1.loadFromFile("vaisseau1.png"))
        return EXIT_FAILURE;
    Sprite perso(vaisseau1);
    posPerso.x=0;
    posPerso.y=0;
    perso.setScale(0.5f,0.5f);
    perso.setPosition(posPerso.x,posPerso.y);
    perso.setOrigin(SIZE/2,SIZE/2);
    posPerso.x=400;
    posPerso.y=300;
    bool up=false,down=false,left=false,right=false;
    // Start the game loop
    while (mouvement.isOpen())
    {
        // Process events
        Event event;
        while (mouvement.pollEvent(event))
        {
            switch(event.type)
            {
            case Event::Closed:
                mouvement.close();
                break;
            case Event::KeyPressed:
                if (event.key.code==Keyboard::Z)
                    up=true;
                if (event.key.code==Keyboard::S)
                    down=true;
                if (event.key.code==Keyboard::D)
                    right=true;
                if (event.key.code==Keyboard::Q)
                    left=true;
                if(event.key.code==Keyboard::LShift)
                    speed=speed/2;
                break;

            case Event::KeyReleased:
                if(event.key.code==Keyboard::Z)
                    up=false;
                if (event.key.code==Keyboard::S)
                    down=false;
                if (event.key.code==Keyboard::D)
                    right=false;
                if (event.key.code==Keyboard::Q)
                    left=false;
                 if(event.key.code==Keyboard::LShift)
                    speed=speed*2;
                break;
            }
        }
        if(up && right)
        {
            posPerso.y=posPerso.y-(speed*2/3);
            posPerso.x=posPerso.x+(speed*2/3);
        }
        else if(up && left)
        {
            posPerso.y=posPerso.y-(speed*2/3);
            posPerso.x=posPerso.x-(speed*2/3);
        }
        else if(down && left)
        {
            posPerso.y=posPerso.y+(speed*2/3);
            posPerso.x=posPerso.x-(speed*2/3);
        }
        else if(down && right)
        {
            posPerso.y=posPerso.y+(speed*2/3);
            posPerso.x=posPerso.x+(speed*2/3);
        }
        else if (up)
            posPerso.y=posPerso.y-speed;
        else if(down)
            posPerso.y=posPerso.y+speed;
        else if (right)
            posPerso.x=posPerso.x+speed;
        else if(left)
            posPerso.x=posPerso.x-speed;

        perso.setPosition(posPerso.x,posPerso.y);

        // Clear screen
        mouvement.clear();

        // Draw the sprite
        mouvement.draw(perso);

        // Update the window
        mouvement.display();
    }

    return EXIT_SUCCESS;
}
