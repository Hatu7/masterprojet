#ifndef AUTRE_HPP_INCLUDED
#define AUTRE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#define TAILLE_FENETRE 600

typedef struct {
    int x;
    int y;
}Point;

int alea(int mini, int maxi);

#endif // AUTRE_HPP_INCLUDED
