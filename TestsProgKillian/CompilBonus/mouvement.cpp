#include <SFML/Graphics.hpp>
#include "mouvement.hpp"
using namespace sf;

void mouvement(Point *pos, int speed)
{
    if (Keyboard::isKeyPressed(Keyboard::LShift))
        speed=speed*0.7;
    if(Keyboard::isKeyPressed(Keyboard::S))
        pos->y +=speed;
    if(Keyboard::isKeyPressed(Keyboard::W))
        pos->y -= speed;
    if(Keyboard::isKeyPressed(Keyboard::A))
        pos->x -= speed;
    if(Keyboard::isKeyPressed(Keyboard::D))
        pos->x += speed;
}
