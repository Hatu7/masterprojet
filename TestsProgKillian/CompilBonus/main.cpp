#include <SFML/Graphics.hpp>
#include <time.h>
#include "projectiles.hpp"
#include "mouvement.hpp"
#include "autre.hpp"
void bonusRalenti(bool * bonus, sf::Clock * clockBonus, int * vitesseVaisseau);
using namespace sf;
int main()
{
    srand(time(NULL));
    int nbProjectile = 0;
    Projectile simple[50];

    RenderWindow app(VideoMode(TAILLE_FENETRE, TAILLE_FENETRE), "Projet");
    app.setFramerateLimit(60);

    Texture imageProjectileSimpleRouge;
    if (!imageProjectileSimpleRouge.loadFromFile("ProjectileSimpleRouge.png"))
        printf("PB de chargement de l'image !\n");
    Texture imageProjectileSimpleBleu;
    if (!imageProjectileSimpleBleu.loadFromFile("ProjectileSimpleBleu.png"))
        printf("PB de chargement de l'image !\n");

    Sprite projectileSimple;


    Point posVaisseau;
    Texture vaisseau1;
    if (!vaisseau1.loadFromFile("vaisseau1.png"))
        return EXIT_FAILURE;
    Sprite vaisseau(vaisseau1);
    posVaisseau.x=400;
    posVaisseau.y=300;
    vaisseau.setScale(0.5f,0.5f);
    vaisseau.setOrigin(50,50);
    vaisseau.setPosition(400,300);
    int vitesseV=5;
    Clock clockBonus1;
    bool bonus1=false;


    Clock clock;
    Event evenement;
    while (app.isOpen())
    {
        while (app.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                app.close();
                break;
            }
        }
        bonusRalenti(&bonus1,&clockBonus1,&vitesseV);
        mouvement(&posVaisseau,vitesseV);

        app.clear();
        if (clock.getElapsedTime().asSeconds() >= (nbProjectile+1) * ECART_SIMPLE)
        {
            creationProjectileSimple(nbProjectile, simple);
            nbProjectile++;
        }

        for (int i = 0; i < nbProjectile; i++)
        {
            affichageProjectileSimple (i, simple, clock, &projectileSimple, &imageProjectileSimpleRouge, &imageProjectileSimpleBleu);
            app.draw(projectileSimple);
        }

        vaisseau.setPosition(posVaisseau.x,posVaisseau.y);
        checkCollision(nbProjectile,simple,posVaisseau);


        app.draw(vaisseau);

        app.display();
    }

    return EXIT_SUCCESS;
}
void bonusRalenti(bool * bonus,sf::Clock * clockBonus, int * vitesseVaisseau)
{
    if (Keyboard::isKeyPressed(Keyboard::Space))
    {
        *bonus=true;
        clockBonus->restart();
    }
    if (clockBonus->getElapsedTime().asSeconds() <= 5 && *bonus==true)
        *vitesseVaisseau=2;
    else
    {
        *vitesseVaisseau=5;
        *bonus=false;
    }
}
