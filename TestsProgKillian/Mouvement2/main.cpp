#include <SFML/Graphics.hpp>
using namespace sf;
typedef struct
{
    int x;
    int y;
} Position;

void mouvement(Position * pos, int speed);

int main()
{
    Position posVaisseau;
    // Create the main window
    RenderWindow app(VideoMode(800, 600), "SFML window");
    // Load a sprite to display
    Texture vaisseau1;
    if (!vaisseau1.loadFromFile("vaisseau1.png"))
        return EXIT_FAILURE;
    Sprite vaisseau(vaisseau1);
    posVaisseau.x=400;
    posVaisseau.y=300;
    vaisseau.setScale(0.5f,0.5f);
    vaisseau.setOrigin(50,50);
    vaisseau.setPosition(400,300);

    // Start the game loop
    while (app.isOpen())
    {
        // Process events
        Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit50
            if (event.type == Event::Closed)
                app.close();
        }
        mouvement(&posVaisseau,100);

        vaisseau.setPosition(posVaisseau.x,posVaisseau.y);
        // Clear screen
        app.clear();

        // Draw the sprite
        app.draw(vaisseau);

        // Update the window
        app.display();
    }

    return EXIT_SUCCESS;
}
void mouvement(Position * pos, int speed)
{
    if(Keyboard::isKeyPressed(Keyboard::S))
        pos->y +=1;
    if(Keyboard::isKeyPressed(Keyboard::W))
        pos->y -= 1;
    if(Keyboard::isKeyPressed(Keyboard::A))
        pos->x -= 1;
    if(Keyboard::isKeyPressed(Keyboard::D))
        pos->x += 1;
}

