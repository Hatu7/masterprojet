#ifndef AUTRE_HPP_INCLUDED
#define AUTRE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#define TAILLE_FENETRE_X 1200
#define TAILLE_FENETRE_Y 800
#define TAILLE_MENU_SCORE 300
using namespace sf;

typedef struct {
    float x;
    float y;
}Point;

int alea(int mini, int maxi);
void updateScore(int difficulte, sf::Clock clock, int *score);
void setOriginAndPosition(Sprite *sprite, float originX, float originY, float positionX, float positionY);
void setOriginAndPositionAndScale(Sprite *sprite, float originX, float originY, float positionX, float positionY, float scaleX, float scaleY);
void setText (Text *text, Font *font, char str[], int size, Color color, float positionX, float positionY);
#endif // AUTRE_HPP_INCLUDED
