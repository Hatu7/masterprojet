#include <SFML/Graphics.hpp>
#include <cmath>
#include "bonus.hpp"
#include "autre.hpp"
using namespace sf;

void creationBonus (bool *bonusPresent, int *bonusType, Clock *apparitionBonus, Sprite *Bonus, Texture *Bonus1, Texture *Bonus2, Texture *Bonus3)
{
    float time = (*apparitionBonus).getElapsedTime().asSeconds();
    if (time > 20)
    {
        (*apparitionBonus).restart();
        time = 0;
        *bonusPresent = false;
        (*Bonus).setColor(Color(255,255,255,255));
    }
    if (*bonusPresent == false && time >= 5 && time <= 5.1)
    {
        *bonusPresent = true;
        *bonusType = alea(0,2);
        (*Bonus).setPosition(alea(10,690), alea(10,790));
        (*Bonus).setColor(Color(255,255,255,255));
    }
    else if (*bonusPresent == true)
    {
        if (time <= 15)
        {
            switch(*bonusType)
            {
            case 0:
                (*Bonus).setTexture(*Bonus1);
                break;
            case 1:
                (*Bonus).setTexture(*Bonus2);
                break;
            case 2:
                (*Bonus).setTexture(*Bonus3);
                break;
            }
        }
        else
            (*Bonus).setColor(Color(255,255,255,128));
    }
    printf("%f %i\n",time,*bonusPresent);
}

void bonusRecup (bool *bonusPresent, int bonusType, int bonus[], Sprite Vaisseau, Sprite Bonus, Clock apparitionBonus)
{
    float timeget = apparitionBonus.getElapsedTime().asSeconds();
    if ((*bonusPresent) == true && Vaisseau.getGlobalBounds().intersects(Bonus.getGlobalBounds()) && timeget > 5.1)
    {
        *bonusPresent = false;
        bonus[bonusType] = 1;
    }
}

void bonusRalenti(bool * active,sf::Clock * clockBonus, int * vitesseVaisseau,int bonus[],bool autre) //A compléter avec le ralentissement de vitesse des projectiles, comme la vitesse est un int j'ai mis 5 et 2 au lieu de faire un vitesse/2
{
    /*if (Keyboard::isKeyPressed(Keyboard::I))
        bonus[0]=1;*/
    if((Keyboard::isKeyPressed(Keyboard::G))&&bonus[0]==1 && autre==false)
    {
        *active=true;
        bonus[0]=0;
        clockBonus->restart();
    }
    if (clockBonus->getElapsedTime().asSeconds() <= 5 && *active==true)
        {
            *vitesseVaisseau=2;
            printf("ralenti");
        }
    else if (autre==false)
    {
        *vitesseVaisseau=5;
        *active=false;
    }
}

void bonusBombe(Projectile simple[], int nbProjectile, int bonus[], Point posVaisseau)
{
    /*if (Keyboard::isKeyPressed(Keyboard::O))
        bonus[1]=1;*/
    if (Keyboard::isKeyPressed(Keyboard::H)&&bonus[1]==1 )
    {
        bonus[1]=0;
        for (int i = 0; i < nbProjectile; i++)
        {
            if (simple[i].position.x<=(posVaisseau.x+TAILLE_VAISSEAU/4 + 100) && simple[i].position.x>=(posVaisseau.x-TAILLE_VAISSEAU/4 - 100) &&
                simple[i].position.y<=(posVaisseau.y+TAILLE_VAISSEAU/4 + 100) && simple[i].position.y>=(posVaisseau.y-TAILLE_VAISSEAU/4 - 100))
            {
                for (int j = i ; j < nbProjectile ; j++)
                {
                    simple[j] = simple[j+1];
                }
                nbProjectile--;
                i--;
            }
        }
    }
}

void bonusVitesse(bool *active,sf::Clock *clockVitesse, int * vitesseVaisseau,int bonus[],bool autre)
{
    /*if (Keyboard::isKeyPressed(Keyboard::P))
        bonus[2]=1;*/
    if (Keyboard::isKeyPressed(Keyboard::J)&&bonus[2]==1 && autre==false)
    {
        *active=true;
        bonus[2]=0;
        clockVitesse->restart();
    }
    if (clockVitesse->getElapsedTime().asSeconds() <=5 && *active==true)
    {
        *vitesseVaisseau=8;
        printf("active");
    }
    else if (autre==false)
    {
        *vitesseVaisseau=5;
        *active=false;
    }
}

