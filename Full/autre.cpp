#include <SFML/Graphics.hpp>
#include "autre.hpp"
using namespace sf;

int alea(int mini, int maxi)
{
    int res;
    res = rand()%(maxi-mini+1) + mini;
    return res;
}

void updateScore(int difficulte, sf::Clock clock, int *score)
{
    switch(difficulte)
    {
    case 1:
        *score = clock.getElapsedTime().asSeconds() * 1000 * 0.5;
        break;
    case 2:
        *score = clock.getElapsedTime().asSeconds() * 1000 * 1;
        break;
    case 3:
        *score = clock.getElapsedTime().asSeconds() * 1000 * 2;
        break;
    }
}

void setOriginAndPosition (Sprite *sprite, float originX, float originY, float positionX, float positionY)
{
    (*sprite).setOrigin(originX, originY);
    (*sprite).setPosition(positionX, positionY);
}

void setOriginAndPositionAndScale (Sprite *sprite, float originX, float originY, float positionX, float positionY, float scaleX, float scaleY)
{
    (*sprite).setOrigin(originX, originY);
    (*sprite).setPosition(positionX, positionY);
    (*sprite).setScale(Vector2f(scaleX, scaleY));
}

void setText (Text *text, Font *font, char str[], int size, Color color, float positionX, float positionY)
{
    (*text).setFont((*font));
    (*text).setString(str);
    (*text).setCharacterSize(size);
    (*text).setFillColor(color);
    (*text).setOrigin((*text).getLocalBounds().width/2, (*text).getLocalBounds().height);
    (*text).setPosition(positionX, positionY);
}
