#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string.h>
#include <iostream>
#include <cmath>

#include "projectiles.hpp"
#include "mouvement.hpp"
#include "autre.hpp"
#include "bonus.hpp"

#define LARGEUR_FENETRE 1200
#define HAUTEUR_FENETRE 800
#define LARGEUR_LOGO 900
#define HAUTEUR_LOGO 300
#define LARGEUR_BOUTON 450
#define HAUTEUR_BOUTON 150
#define TAILLE_BOUTONCARRE 105
#define VIDE_BUTTON 27
#define TAILLE_TEXTE_DESC 25
#define TAILLE_TEXTE_MENU 50
#define TAILLE_TEXTE_TITRE 100
#define TAILLE_VAISSEAU 200
#define LARGEUR_BARRE 1001
#define HAUTEUR_BARRE 51
#define TAILLE_PETITBOUTON 55
#define TAILLE_TEXTE_TITREBARRE 40
#define TAILLE_TEXTE_TITRELONGBARRE 30
#define TAILLE_TEXTE_SMALLTEXTEBARRE 20
#define TAILLE_ROND 84

using namespace sf;
using namespace std;

typedef struct
{
    int id;
    char nom[30];

} Vaisseau;

typedef struct
{
    Vaisseau vaisseau;
    char nom[30];

} Joueur;

int menu = 1;
int soundState = 1;
int volume = 25;
const char *NOM_VAISSEAU[5] = {"Starship", "Eagleship", "Discoveryship", "Prawnship", "Nostroship"};
int scores[3] = {0, 0, 0};

int difficulte = 0;
int score = 0;
int vies = 2;
int bonus[3] = {0,0,0};
int gameover = 0;

int main()
{
    srand(time(NULL));
    int nbProjectile = 0;
    int vies = 3;
    Projectile simple[50];
    Point posVaisseau;

    posVaisseau.x=400;
    posVaisseau.y=300;


    bool bonus1; //Booléen pour indiqué si le bonus est allumé ou éteint (True= active et false=désactivé)
    bool bonus2;
    int vitesseV=5;
    Clock clockBonus1; //Clock pour les 5s du bonus
    Clock clock;
    Clock invincibilite;
    Clock apparitionBonus;
    Clock clockVitesse;
    bool bonusPresent = false;
    int bonusType = 0;

    // Main menu window
    RenderWindow window(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "SPACE ESCAPE");
    window.setFramerateLimit(60);

    // Initialiser les paramètres de base
    Joueur joueur;
    strcpy(joueur.nom, "Joueur");
    joueur.vaisseau.id = 1;
    strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[joueur.vaisseau.id-1]);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////    GAME SET UP                                                                                                               ////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Textures declaration
    Texture background, logo, button, buttonPressed, trophyButton, trophyButtonPressed, bar, bar25, bar50, bar75, bar100, back, forward,
            backPressed, forwardPressed, vaisseau1, vaisseau2, vaisseau3, vaisseau4, vaisseau5, vaisseau1Dark, vaisseau2Dark, vaisseau3Dark,
            vaisseau4Dark, vaisseau5Dark, vaisseau1Choice, vaisseau2Choice, vaisseau3Choice, vaisseau4Choice, vaisseau5Choice, round, roundPressed,
            imageProjectileSimpleRouge, imageProjectileSimpleBleu, inGamebackground, round01, box01, star, starEmpty, round02, bonus1Time,
            bonus2Bomb, bonus3Speed, bonus1TimeEmpty, bonus2BombEmpty, bonus3SpeedEmpty, explo;

    // Load Files
    background.loadFromFile("Ressources/Images/background.png");
    logo.loadFromFile("Ressources/Images/logo.png");
    button.loadFromFile("Ressources/Images/button.png");
    buttonPressed.loadFromFile("Ressources/Images/buttonPressed.png");
    vaisseau1.loadFromFile("Ressources/Images/vaisseau1.png");
    vaisseau2.loadFromFile("Ressources/Images/vaisseau2.png");
    vaisseau3.loadFromFile("Ressources/Images/vaisseau3.png");
    vaisseau4.loadFromFile("Ressources/Images/vaisseau4.png");
    vaisseau5.loadFromFile("Ressources/Images/vaisseau5.png");
    vaisseau1Dark.loadFromFile("Ressources/Images/vaisseau1Dark.png");
    vaisseau2Dark.loadFromFile("Ressources/Images/vaisseau2Dark.png");
    vaisseau3Dark.loadFromFile("Ressources/Images/vaisseau3Dark.png");
    vaisseau4Dark.loadFromFile("Ressources/Images/vaisseau4Dark.png");
    vaisseau5Dark.loadFromFile("Ressources/Images/vaisseau5Dark.png");
    vaisseau1Choice.loadFromFile("Ressources/Images/vaisseau1Choice.png");
    vaisseau2Choice.loadFromFile("Ressources/Images/vaisseau2Choice.png");
    vaisseau3Choice.loadFromFile("Ressources/Images/vaisseau3Choice.png");
    vaisseau4Choice.loadFromFile("Ressources/Images/vaisseau4Choice.png");
    vaisseau5Choice.loadFromFile("Ressources/Images/vaisseau5Choice.png");
    trophyButton.loadFromFile("Ressources/Images/trophy.png");
    trophyButtonPressed.loadFromFile("Ressources/Images/trophyPressed.png");
    bar.loadFromFile("Ressources/Images/bar.png");
    bar25.loadFromFile("Ressources/Images/bar25.png");
    bar50.loadFromFile("Ressources/Images/bar50.png");
    bar75.loadFromFile("Ressources/Images/bar75.png");
    bar100.loadFromFile("Ressources/Images/bar100.png");
    back.loadFromFile("Ressources/Images/back.png");
    forward.loadFromFile("Ressources/Images/forward.png");
    backPressed.loadFromFile("Ressources/Images/backPressed.png");
    forwardPressed.loadFromFile("Ressources/Images/forwardPressed.png");
    round.loadFromFile("Ressources/Images/round01.png");
    roundPressed.loadFromFile("Ressources/Images/round01Pressed.png");
    imageProjectileSimpleRouge.loadFromFile("Ressources/Images/ProjectileSimpleRouge.png");
    imageProjectileSimpleBleu.loadFromFile("Ressources/Images/ProjectileSimpleBleu.png");
    inGamebackground.loadFromFile("Ressources/Images/inGameBackground.png");
    round01.loadFromFile("Ressources/Images/round01.png");
    box01.loadFromFile("Ressources/Images/box01.png");
    star.loadFromFile("Ressources/Images/star.png");
    starEmpty.loadFromFile("Ressources/Images/starEmpty.png");
    round02.loadFromFile("Ressources/Images/round02.png");
    bonus1Time.loadFromFile("Ressources/Images/bonus1Time.png");
    bonus2Bomb.loadFromFile("Ressources/Images/bonus2Bomb.png");
    bonus3Speed.loadFromFile("Ressources/Images/bonus3Speed.png");
    bonus1TimeEmpty.loadFromFile("Ressources/Images/bonus1TimeEmpty.png");
    bonus2BombEmpty.loadFromFile("Ressources/Images/bonus2BombEmpty.png");
    bonus3SpeedEmpty.loadFromFile("Ressources/Images/bonus3SpeedEmpty.png");
    explo.loadFromFile("Ressources/Images/explo.png");

    // Sprites declaration
    Sprite backgroundSprite(background), logoSprite, startButtonImage, optionsButtonImage, quitButtonImage, vaisseauSprite, trophySprite,
           backButtonImage, barSprite, backSprite, forwardSprite, vaisseau(vaisseau1), bonusSprite, vaisseau1Sprite, vaisseau2Sprite,
           vaisseau3Sprite, vaisseau4Sprite, vaisseau5Sprite, validationButtonImage, difficulte1Sprite, difficulte2Sprite, difficulte3Sprite,
           projectileSimple, inGamebackgroundSprite(inGamebackground), levelRoundSprite, scoreBoxSprite, starEmpty1, starEmpty2, starEmpty3,
           star1, star2, star3, bonus1RoundSprite, bonus2RoundSprite, bonus3RoundSprite, bonus1Sprite, bonus2Sprite, bonus3Sprite, bonus1SpriteEmpty,
           bonus2SpriteEmpty, bonus3SpriteEmpty, returnToMenuButtonImage, playAgainButtonImage, exploSprite;

    // Setting the texture for the sprites
    logoSprite.setTexture(logo);
    startButtonImage.setTexture(button);
    optionsButtonImage.setTexture(button);
    quitButtonImage.setTexture(button);
    trophySprite.setTexture(trophyButton);
    backButtonImage.setTexture(button);
    backSprite.setTexture(back);
    forwardSprite.setTexture(forward);
    difficulte1Sprite.setTexture(round);
    difficulte2Sprite.setTexture(round);
    difficulte3Sprite.setTexture(round);
    levelRoundSprite.setTexture(round01);
    scoreBoxSprite.setTexture(box01);
    starEmpty1.setTexture(starEmpty);
    starEmpty2.setTexture(starEmpty);
    starEmpty3.setTexture(starEmpty);
    star1.setTexture(star);
    star2.setTexture(star);
    star3.setTexture(star);
    bonus1RoundSprite.setTexture(round02);
    bonus2RoundSprite.setTexture(round02);
    bonus3RoundSprite.setTexture(round02);
    bonus1Sprite.setTexture(bonus1Time);
    bonus2Sprite.setTexture(bonus2Bomb);
    bonus3Sprite.setTexture(bonus3Speed);
    bonus1SpriteEmpty.setTexture(bonus1TimeEmpty);
    bonus2SpriteEmpty.setTexture(bonus2BombEmpty);
    bonus3SpriteEmpty.setTexture(bonus3SpeedEmpty);
    returnToMenuButtonImage.setTexture(button);
    playAgainButtonImage.setTexture(button);
    exploSprite.setTexture(explo);

    switch(volume)
    {
    case 0:
        barSprite.setTexture(bar);
        break;
    case 25:
        barSprite.setTexture(bar25);
        break;
    case 50:
        barSprite.setTexture(bar50);
        break;
    case 75:
        barSprite.setTexture(bar75);
        break;
    case 100:
        barSprite.setTexture(bar100);
        break;
    }

    vaisseau1Sprite.setTexture(vaisseau1Dark);
    vaisseau2Sprite.setTexture(vaisseau2Dark);
    vaisseau3Sprite.setTexture(vaisseau3Dark);
    vaisseau4Sprite.setTexture(vaisseau4Dark);
    vaisseau5Sprite.setTexture(vaisseau5Dark);
    validationButtonImage.setTexture(button);

    switch(joueur.vaisseau.id)
    {
    case 1:
        vaisseauSprite.setTexture(vaisseau1);
        vaisseau1Sprite.setTexture(vaisseau1Choice);
        break;
    case 2:
        vaisseauSprite.setTexture(vaisseau2);
        vaisseau2Sprite.setTexture(vaisseau2Choice);
        break;
    case 3:
        vaisseauSprite.setTexture(vaisseau3);
        vaisseau3Sprite.setTexture(vaisseau3Choice);
        break;
    case 4:
        vaisseauSprite.setTexture(vaisseau4);
        vaisseau4Sprite.setTexture(vaisseau4Choice);
        break;
    case 5:
        vaisseauSprite.setTexture(vaisseau5);
        vaisseau5Sprite.setTexture(vaisseau5Choice);
        break;
    }

    // Setting origin, position and scale for the sprites
    setOriginAndPosition(&logoSprite, LARGEUR_LOGO/2, HAUTEUR_LOGO/2, LARGEUR_FENETRE/2, 0.02*HAUTEUR_FENETRE+HAUTEUR_LOGO/2);
    setOriginAndPosition(&startButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);
    setOriginAndPosition(&optionsButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON);
    setOriginAndPosition(&quitButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);
    setOriginAndPosition(&vaisseauSprite, TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2, 0.1*LARGEUR_FENETRE, HAUTEUR_FENETRE-TAILLE_VAISSEAU+0.10*HAUTEUR_FENETRE);
    setOriginAndPosition(&trophySprite, TAILLE_BOUTONCARRE/2, TAILLE_BOUTONCARRE/2, LARGEUR_FENETRE-TAILLE_BOUTONCARRE/2, HAUTEUR_FENETRE-TAILLE_BOUTONCARRE/2);
    setOriginAndPosition(&vaisseau1Sprite, TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2, LARGEUR_FENETRE/2 - 2*(TAILLE_VAISSEAU) - 2*(LARGEUR_FENETRE/30), HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);
    setOriginAndPosition(&vaisseau2Sprite, TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2, LARGEUR_FENETRE/2 - TAILLE_VAISSEAU - LARGEUR_FENETRE/30, HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);
    setOriginAndPosition(&vaisseau3Sprite, TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);
    setOriginAndPosition(&vaisseau4Sprite, TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2, LARGEUR_FENETRE/2 + TAILLE_VAISSEAU + LARGEUR_FENETRE/30, HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);
    setOriginAndPosition(&vaisseau5Sprite, TAILLE_VAISSEAU/2, TAILLE_VAISSEAU/2, LARGEUR_FENETRE/2 + 2*(TAILLE_VAISSEAU) + 2*(LARGEUR_FENETRE/30), HAUTEUR_FENETRE*0.92-TAILLE_VAISSEAU);
    setOriginAndPosition(&validationButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE - HAUTEUR_BOUTON + HAUTEUR_FENETRE/10);
    setOriginAndPosition(&backButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);
    setOriginAndPosition(&barSprite, LARGEUR_BARRE/2, HAUTEUR_BARRE/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+HAUTEUR_BARRE*2);
    setOriginAndPosition(&backSprite, TAILLE_PETITBOUTON/2, TAILLE_PETITBOUTON/2, TAILLE_PETITBOUTON, barSprite.getPosition().y);
    setOriginAndPosition(&forwardSprite, TAILLE_PETITBOUTON/2, TAILLE_PETITBOUTON/2, LARGEUR_FENETRE-TAILLE_PETITBOUTON, barSprite.getPosition().y);
    setOriginAndPosition(&levelRoundSprite, levelRoundSprite.getLocalBounds().width/2, levelRoundSprite.getLocalBounds().height, 1050, 235);
    setOriginAndPosition(&scoreBoxSprite, scoreBoxSprite.getLocalBounds().width/2, scoreBoxSprite.getLocalBounds().height, 1050, 360);
    setOriginAndPosition(&starEmpty1, starEmpty1.getLocalBounds().width/2, starEmpty1.getLocalBounds().height, 965, 515);
    setOriginAndPosition(&starEmpty2, starEmpty2.getLocalBounds().width/2, starEmpty2.getLocalBounds().height, 1050, 515);
    setOriginAndPosition(&starEmpty3, starEmpty3.getLocalBounds().width/2, starEmpty3.getLocalBounds().height, 1135, 515);
    setOriginAndPosition(&star1, star1.getLocalBounds().width/2, star1.getLocalBounds().height, 965, 515);
    setOriginAndPosition(&star2, star2.getLocalBounds().width/2, star2.getLocalBounds().height, 1050, 515);
    setOriginAndPosition(&star3, star3.getLocalBounds().width/2, star3.getLocalBounds().height, 1135, 515);
    setOriginAndPosition(&bonus1RoundSprite, bonus1RoundSprite.getLocalBounds().width/2, bonus1RoundSprite.getLocalBounds().height, 965, 680);
    setOriginAndPosition(&bonus2RoundSprite, bonus2RoundSprite.getLocalBounds().width/2, bonus2RoundSprite.getLocalBounds().height, 1050, 680);
    setOriginAndPosition(&bonus3RoundSprite, bonus3RoundSprite.getLocalBounds().width/2, bonus3RoundSprite.getLocalBounds().height, 1135, 680);
    setOriginAndPosition(&returnToMenuButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/5, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);
    setOriginAndPosition(&playAgainButtonImage, LARGEUR_BOUTON/2, HAUTEUR_BOUTON/2, LARGEUR_FENETRE/5+LARGEUR_BOUTON*0.95, HAUTEUR_FENETRE/2+HAUTEUR_BOUTON*2);

    setOriginAndPositionAndScale(&difficulte1Sprite, TAILLE_ROND/2, TAILLE_ROND/2, LARGEUR_FENETRE/3, HAUTEUR_FENETRE/1.5, 2, 2);
    setOriginAndPositionAndScale(&difficulte2Sprite, TAILLE_ROND/2, TAILLE_ROND/2, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/1.5, 2, 2);
    setOriginAndPositionAndScale(&difficulte3Sprite, TAILLE_ROND/2, TAILLE_ROND/2, LARGEUR_FENETRE-LARGEUR_FENETRE/3, HAUTEUR_FENETRE/1.5, 2, 2);
    setOriginAndPositionAndScale(&vaisseau, 50, 50, 400, 300, 0.5f,0.5f);
    setOriginAndPositionAndScale(&bonus1Sprite, bonus1Sprite.getLocalBounds().width/2, bonus1Sprite.getLocalBounds().height, 965, 662, 0.8, 0.8);
    setOriginAndPositionAndScale(&bonus2Sprite, bonus2Sprite.getLocalBounds().width/2, bonus2Sprite.getLocalBounds().height, 1050, 662, 0.8, 0.8);
    setOriginAndPositionAndScale(&bonus3Sprite, bonus3Sprite.getLocalBounds().width/2, bonus3Sprite.getLocalBounds().height, 1135, 659, 0.8, 0.8);
    setOriginAndPositionAndScale(&bonus1SpriteEmpty, bonus1SpriteEmpty.getLocalBounds().width/2, bonus1SpriteEmpty.getLocalBounds().height, 965, 662, 0.8, 0.8);
    setOriginAndPositionAndScale(&bonus2SpriteEmpty, bonus2SpriteEmpty.getLocalBounds().width/2, bonus2SpriteEmpty.getLocalBounds().height, 1050, 662, 0.8, 0.8);
    setOriginAndPositionAndScale(&bonus3SpriteEmpty, bonus3SpriteEmpty.getLocalBounds().width/2, bonus3SpriteEmpty.getLocalBounds().height, 1135, 659, 0.8, 0.8);

    // Setting the texts
    Font font, fontSpace;
    font.loadFromFile("Ressources/Fonts/SWEETMANGO.ttf");
    fontSpace.loadFromFile("Ressources/Fonts/SPACE.ttf");

    Text startText, optionsText, quitText, choixVaisseauText, validationText, nomVaisseauText, backText, volumeChoiceText, volumeNumberText,
         scoresText, score1Text, score2Text, score3Text, difficulteChoiceText, difficulte1Text, difficulte2Text, difficulte3Text, titleText,
         difficulteText, difficulteNumberText, scoreText, scoreNumberText, viesText, bonusText, gameOverText, gameOverScoreText,
         returnToMenuText, playAgainText;

    setText(&startText, &font, "Jouer", TAILLE_TEXTE_MENU, Color::White, startButtonImage.getPosition().x, startButtonImage.getPosition().y);
    setText(&optionsText, &font, "Options", TAILLE_TEXTE_MENU, Color::White, optionsButtonImage.getPosition().x, optionsButtonImage.getPosition().y+5);
    setText(&quitText, &font, "Quitter", TAILLE_TEXTE_MENU, Color::White, quitButtonImage.getPosition().x, quitButtonImage.getPosition().y);
    setText(&choixVaisseauText, &font, "Choisissez votre vaisseau", TAILLE_TEXTE_TITRE, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE*0.95/2);
    setText(&validationText, &font, "Valider", TAILLE_TEXTE_MENU, Color::White, validationButtonImage.getPosition().x, validationButtonImage.getPosition().y);
    char nomVaisseau[30];
    sprintf(nomVaisseau, "%s", NOM_VAISSEAU[joueur.vaisseau.id-1]);
    setText(&nomVaisseauText, &font, nomVaisseau, TAILLE_TEXTE_DESC, Color::White, vaisseau1Sprite.getPosition().x - nomVaisseauText.getLocalBounds().width/2, vaisseau1Sprite.getPosition().y);
    setText(&backText, &font, "Retour", TAILLE_TEXTE_MENU, Color::White, backButtonImage.getPosition().x, backButtonImage.getPosition().y);
    setText(&volumeChoiceText, &font, "Volume", TAILLE_TEXTE_TITRE, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE*0.95/2);
    setText(&volumeNumberText, &font, "", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/1.585);
    setText(&scoresText, &font, "Meilleurs scores", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2.4);
    setText(&score1Text, &font, "", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);
    setText(&score2Text, &font, "", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+50);
    setText(&score3Text, &font, "", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2+100);
    setText(&difficulteChoiceText, &font, "Choix de la difficulte", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2, HAUTEUR_FENETRE/2);
    setText(&difficulte1Text, &fontSpace, "1", TAILLE_TEXTE_TITRE, Color::White, difficulte1Sprite.getPosition().x, difficulte1Sprite.getPosition().y+20);
    setText(&difficulte2Text, &fontSpace, "2", TAILLE_TEXTE_TITRE, Color::White, difficulte2Sprite.getPosition().x, difficulte2Sprite.getPosition().y+20);
    setText(&difficulte3Text, &fontSpace, "3", TAILLE_TEXTE_TITRE, Color::White, difficulte3Sprite.getPosition().x, difficulte3Sprite.getPosition().y+20);
    setText(&titleText, &fontSpace, "Infos", TAILLE_TEXTE_TITREBARRE, Color::White, 1050, 52);
    setText(&difficulteText, &fontSpace, "Difficulte", TAILLE_TEXTE_TITRELONGBARRE, Color::White, 1050, 130);
    setText(&difficulteNumberText, &fontSpace, "", TAILLE_TEXTE_TITREBARRE, Color::White, 1047, 200);
    setText(&scoreText, &fontSpace, "Score", TAILLE_TEXTE_TITREBARRE, Color::White, 1050, 290);
    setText(&scoreNumberText, &fontSpace, "", TAILLE_TEXTE_SMALLTEXTEBARRE, Color::White, 1050, 342);
    setText(&viesText, &fontSpace, "Vies", TAILLE_TEXTE_TITREBARRE, Color::White, 1050, 410);
    setText(&bonusText, &fontSpace, "Bonus", TAILLE_TEXTE_TITREBARRE, Color::White, 1050, 565);
    setText(&gameOverText, &fontSpace, "GAME OVER", TAILLE_TEXTE_TITRE, Color::White, LARGEUR_FENETRE/2.7, HAUTEUR_FENETRE*0.95/2);
    setText(&gameOverScoreText, &fontSpace, "GAME OVER", TAILLE_TEXTE_MENU, Color::White, LARGEUR_FENETRE/2.7, HAUTEUR_FENETRE/1.7);
    setText(&returnToMenuText, &font, "Retour au menu", TAILLE_TEXTE_MENU, Color::White, returnToMenuButtonImage.getPosition().x, returnToMenuButtonImage.getPosition().y);
    setText(&playAgainText, &font, "Quitter", TAILLE_TEXTE_MENU, Color::White, playAgainButtonImage.getPosition().x, playAgainButtonImage.getPosition().y);

    // Son
    SoundBuffer buffer;
    buffer.loadFromFile("Ressources/Sons/music.wav");

    Sound sound;
    sound.setBuffer(buffer);
    sound.setVolume(volume);
    sound.play();
    sound.setLoop(true);

    // Main loop
    while(window.isOpen())
    {
        Event event;
        while(window.pollEvent(event))
        {
            if(event.type == Event::Closed)
                window.close();
            if (event.type == Event::MouseMoved)
            {
                if(menu == 1) // SI ON EST DANS LE MENU PRINCIPAL
                {
                    if(event.mouseMove.x >= startButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= startButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= startButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= startButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        startButtonImage.setTexture(buttonPressed);
                        startText.setPosition(startButtonImage.getPosition().x+2, startButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        startButtonImage.setTexture(button);
                        startText.setPosition(startButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= optionsButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= optionsButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= optionsButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= optionsButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        optionsButtonImage.setTexture(buttonPressed);
                        optionsText.setPosition(optionsButtonImage.getPosition().x+2, optionsButtonImage.getPosition().y+3);
                    }
                    else
                    {
                        optionsButtonImage.setTexture(button);
                        optionsText.setPosition(optionsButtonImage.getPosition().x, optionsButtonImage.getPosition().y+5);
                    }

                    if(event.mouseMove.x >= quitButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= quitButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= quitButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= quitButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        quitButtonImage.setTexture(buttonPressed);
                        quitText.setPosition(quitButtonImage.getPosition().x+2, quitButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        quitButtonImage.setTexture(button);
                        quitText.setPosition(quitButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= vaisseauSprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseauSprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseauSprite.getPosition().y-TAILLE_VAISSEAU/2 && event.mouseMove.y <= vaisseauSprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1Dark);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2Dark);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3Dark);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4Dark);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5Dark);
                            break;
                        }
                    }
                    else
                    {
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            break;
                        }
                    }

                    if(event.mouseMove.x >= trophySprite.getPosition().x-TAILLE_BOUTONCARRE/2 && event.mouseMove.x <= trophySprite.getPosition().x+TAILLE_BOUTONCARRE/2 && event.mouseMove.y >= trophySprite.getPosition().y-TAILLE_BOUTONCARRE/2 && event.mouseMove.y <= trophySprite.getPosition().y+TAILLE_BOUTONCARRE/2)
                        trophySprite.setTexture(trophyButtonPressed);
                    else
                        trophySprite.setTexture(trophyButton);
                }
                else if(menu == 2) // SI ON EST DANS LE MENU OPTIONS
                {
                    if(event.mouseMove.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        backButtonImage.setTexture(buttonPressed);
                        backText.setPosition(backButtonImage.getPosition().x+2, backButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        backButtonImage.setTexture(button);
                        backText.setPosition(backButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= backSprite.getPosition().x-TAILLE_PETITBOUTON/2 && event.mouseMove.x <= backSprite.getPosition().x+TAILLE_PETITBOUTON/2 && event.mouseMove.y >= backSprite.getPosition().y-TAILLE_PETITBOUTON/2 && event.mouseMove.y <= backSprite.getPosition().y+TAILLE_PETITBOUTON/2)
                        backSprite.setTexture(backPressed);
                    else
                        backSprite.setTexture(back);

                    if(event.mouseMove.x >= forwardSprite.getPosition().x-TAILLE_PETITBOUTON/2 && event.mouseMove.x <= forwardSprite.getPosition().x+TAILLE_PETITBOUTON/2 && event.mouseMove.y >= forwardSprite.getPosition().y-TAILLE_PETITBOUTON/2 && event.mouseMove.y <= forwardSprite.getPosition().y+TAILLE_PETITBOUTON/2)
                        forwardSprite.setTexture(forwardPressed);
                    else
                        forwardSprite.setTexture(forward);
                }
                else if(menu == 3) // SI ON EST DANS LE MENU CHOIX VAISSEAU
                {
                    if(event.mouseMove.x >= vaisseau1Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau1Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau1Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau1Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[0]);
                        nomVaisseauText.setOrigin(nomVaisseauText.getLocalBounds().width/2, nomVaisseauText.getLocalBounds().height);
                        nomVaisseauText.setPosition(vaisseau1Sprite.getPosition().x, vaisseau1Sprite.getPosition().y+TAILLE_VAISSEAU/1.5);
                        vaisseau1Sprite.setTexture(vaisseau1);
                    }
                    else
                        vaisseau1Sprite.setTexture(vaisseau1Dark);

                    if(event.mouseMove.x >= vaisseau2Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau2Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau2Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau2Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[1]);
                        nomVaisseauText.setOrigin(nomVaisseauText.getLocalBounds().width/2, nomVaisseauText.getLocalBounds().height);
                        nomVaisseauText.setPosition(vaisseau2Sprite.getPosition().x, vaisseau2Sprite.getPosition().y+TAILLE_VAISSEAU/1.5);
                        vaisseau2Sprite.setTexture(vaisseau2);
                    }
                    else
                        vaisseau2Sprite.setTexture(vaisseau2Dark);

                    if(event.mouseMove.x >= vaisseau3Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau3Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau3Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau3Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[2]);
                        nomVaisseauText.setOrigin(nomVaisseauText.getLocalBounds().width/2, nomVaisseauText.getLocalBounds().height);
                        nomVaisseauText.setPosition(vaisseau3Sprite.getPosition().x, vaisseau3Sprite.getPosition().y+TAILLE_VAISSEAU/1.5);
                        vaisseau3Sprite.setTexture(vaisseau3);
                    }
                    else
                        vaisseau3Sprite.setTexture(vaisseau3Dark);

                    if(event.mouseMove.x >= vaisseau4Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau4Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau4Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau4Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[3]);
                        nomVaisseauText.setOrigin(nomVaisseauText.getLocalBounds().width/2, nomVaisseauText.getLocalBounds().height);
                        nomVaisseauText.setPosition(vaisseau4Sprite.getPosition().x, vaisseau4Sprite.getPosition().y+TAILLE_VAISSEAU/1.5);
                        vaisseau4Sprite.setTexture(vaisseau4);
                    }
                    else
                        vaisseau4Sprite.setTexture(vaisseau4Dark);

                    if(event.mouseMove.x >= vaisseau5Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseMove.x <= vaisseau5Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseMove.y >= vaisseau5Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseMove.y <= vaisseau5Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        nomVaisseauText.setString(NOM_VAISSEAU[4]);
                        nomVaisseauText.setOrigin(nomVaisseauText.getLocalBounds().width/2, nomVaisseauText.getLocalBounds().height);
                        nomVaisseauText.setPosition(vaisseau5Sprite.getPosition().x, vaisseau5Sprite.getPosition().y+TAILLE_VAISSEAU/1.5);
                        vaisseau5Sprite.setTexture(vaisseau5);
                    }
                    else
                        vaisseau5Sprite.setTexture(vaisseau5Dark);

                    switch(joueur.vaisseau.id)
                    {
                    case 1:
                        vaisseau1Sprite.setTexture(vaisseau1Choice);
                        break;
                    case 2:
                        vaisseau2Sprite.setTexture(vaisseau2Choice);
                        break;
                    case 3:
                        vaisseau3Sprite.setTexture(vaisseau3Choice);
                        break;
                    case 4:
                        vaisseau4Sprite.setTexture(vaisseau4Choice);
                        break;
                    case 5:
                        vaisseau5Sprite.setTexture(vaisseau5Choice);
                        break;
                    }

                    if(event.mouseMove.x >= validationButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= validationButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= validationButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= validationButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        validationButtonImage.setTexture(buttonPressed);
                        validationText.setPosition(validationButtonImage.getPosition().x+2, validationButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        validationButtonImage.setTexture(button);
                        validationText.setPosition(validationButtonImage.getPosition());
                    }
                }
                else if(menu == 4) // SI ON EST DANS LE MENU SCORES
                {
                    if(event.mouseMove.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        backButtonImage.setTexture(buttonPressed);
                        backText.setPosition(backButtonImage.getPosition().x+2, backButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        backButtonImage.setTexture(button);
                        backText.setPosition(backButtonImage.getPosition());
                    }
                }
                else if(menu == 5) // SI ON EST DANS LE MENU CHOIX DIFFICULTE
                {
                    if(event.mouseMove.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        backButtonImage.setTexture(buttonPressed);
                        backText.setPosition(backButtonImage.getPosition().x+2, backButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        backButtonImage.setTexture(button);
                        backText.setPosition(backButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= difficulte1Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseMove.x <= difficulte1Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseMove.y >= difficulte1Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseMove.y <= difficulte1Sprite.getPosition().y+TAILLE_ROND/2)
                        difficulte1Sprite.setTexture(roundPressed);
                    else
                        difficulte1Sprite.setTexture(round);

                    if(event.mouseMove.x >= difficulte2Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseMove.x <= difficulte2Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseMove.y >= difficulte2Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseMove.y <= difficulte2Sprite.getPosition().y+TAILLE_ROND/2)
                        difficulte2Sprite.setTexture(roundPressed);
                    else
                        difficulte2Sprite.setTexture(round);

                    if(event.mouseMove.x >= difficulte3Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseMove.x <= difficulte3Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseMove.y >= difficulte3Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseMove.y <= difficulte3Sprite.getPosition().y+TAILLE_ROND/2)
                        difficulte3Sprite.setTexture(roundPressed);
                    else
                        difficulte3Sprite.setTexture(round);
                }
                else if(menu == 99 && gameover == 1)
                {
                    if(event.mouseMove.x >= returnToMenuButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= returnToMenuButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= returnToMenuButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= returnToMenuButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        returnToMenuButtonImage.setTexture(buttonPressed);
                        returnToMenuText.setPosition(returnToMenuButtonImage.getPosition().x+2, returnToMenuButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        returnToMenuButtonImage.setTexture(button);
                        returnToMenuText.setPosition(returnToMenuButtonImage.getPosition());
                    }

                    if(event.mouseMove.x >= playAgainButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseMove.x <= playAgainButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseMove.y >= playAgainButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseMove.y <= playAgainButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        playAgainButtonImage.setTexture(buttonPressed);
                        playAgainText.setPosition(playAgainButtonImage.getPosition().x+2, playAgainButtonImage.getPosition().y-2);
                    }
                    else
                    {
                        playAgainButtonImage.setTexture(button);
                        playAgainText.setPosition(playAgainButtonImage.getPosition());
                    }
                }
            }
            if (event.mouseButton.button == Mouse::isButtonPressed(Mouse::Left))
            {
                if(menu == 1) // SI ON EST DANS LE MENU PRINCIPAL
                {
                    if(event.mouseButton.x >= startButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= startButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= startButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= startButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 5;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                        vies = 3;
                        nbProjectile = 0;
                        gameover=0;
                        bonusPresent == false;
                        bonus[0] = 0;
                        bonus[1] = 0;
                        bonus[2] = 0;
                    }

                    if(event.mouseButton.x >= optionsButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= optionsButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= optionsButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= optionsButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 2;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= vaisseauSprite.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= vaisseauSprite.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= vaisseauSprite.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= vaisseauSprite.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 3;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= trophySprite.getPosition().x-TAILLE_BOUTONCARRE && event.mouseButton.x <= trophySprite.getPosition().x+TAILLE_BOUTONCARRE && event.mouseButton.y >= trophySprite.getPosition().y-TAILLE_BOUTONCARRE && event.mouseButton.y <= trophySprite.getPosition().y+TAILLE_BOUTONCARRE)
                    {
                        menu = 4;
                        nomVaisseauText.setPosition(LARGEUR_FENETRE, HAUTEUR_FENETRE);
                    }

                    if(event.mouseButton.x >= quitButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= quitButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= quitButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= quitButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                        window.close();
                }
                else if (menu == 2) // SI ON EST DANS LE MENU OPTIONS
                {
                    if(event.mouseButton.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseauSprite.getPosition().x, vaisseauSprite.getPosition().y - vaisseauSprite.getLocalBounds().height/1.4);
                    }

                    if(event.mouseButton.x >= backSprite.getPosition().x-TAILLE_PETITBOUTON && event.mouseButton.x <= backSprite.getPosition().x+TAILLE_PETITBOUTON && event.mouseButton.y >= backSprite.getPosition().y-TAILLE_PETITBOUTON && event.mouseButton.y <= backSprite.getPosition().y+TAILLE_PETITBOUTON)
                    {
                        if(volume != 0)
                            volume = volume-25;
                        sound.setVolume(volume);
                        switch(volume)
                        {
                        case 0:
                            barSprite.setTexture(bar);
                            break;
                        case 25:
                            barSprite.setTexture(bar25);
                            break;
                        case 50:
                            barSprite.setTexture(bar50);
                            break;
                        case 75:
                            barSprite.setTexture(bar75);
                            break;
                        case 100:
                            barSprite.setTexture(bar100);
                            break;
                        }
                    }

                    if(event.mouseButton.x >= forwardSprite.getPosition().x-TAILLE_PETITBOUTON && event.mouseButton.x <= forwardSprite.getPosition().x+TAILLE_PETITBOUTON && event.mouseButton.y >= forwardSprite.getPosition().y-TAILLE_PETITBOUTON && event.mouseButton.y <= forwardSprite.getPosition().y+TAILLE_PETITBOUTON)
                    {
                        if(volume != 100)
                            volume = volume+25;
                        sound.setVolume(volume);
                        switch(volume)
                        {
                        case 0:
                            barSprite.setTexture(bar);
                            break;
                        case 25:
                            barSprite.setTexture(bar25);
                            break;
                        case 50:
                            barSprite.setTexture(bar50);
                            break;
                        case 75:
                            barSprite.setTexture(bar75);
                            break;
                        case 100:
                            barSprite.setTexture(bar100);
                            break;
                        }
                    }
                }
                else if(menu == 3) // SI ON EST DANS LE MENU CHOIX VAISSEAU
                {
                    vaisseau1Sprite.setTexture(vaisseau1Dark);
                    vaisseau2Sprite.setTexture(vaisseau2Dark);
                    vaisseau3Sprite.setTexture(vaisseau3Dark);
                    vaisseau4Sprite.setTexture(vaisseau4Dark);
                    vaisseau5Sprite.setTexture(vaisseau5Dark);

                    if(event.mouseButton.x >= vaisseau1Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau1Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau1Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau1Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 1;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[0]);
                        vaisseau1Sprite.setTexture(vaisseau1Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau2Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau2Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau2Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau2Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 2;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[1]);
                        vaisseau2Sprite.setTexture(vaisseau2Choice);
                        vaisseau.setTexture(vaisseau2);
                    }
                    else if(event.mouseButton.x >= vaisseau3Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau3Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau3Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau3Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 3;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[2]);
                        vaisseau3Sprite.setTexture(vaisseau3Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau4Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau4Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau4Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau4Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 4;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[3]);
                        vaisseau4Sprite.setTexture(vaisseau4Choice);
                    }
                    else if(event.mouseButton.x >= vaisseau5Sprite.getPosition().x-TAILLE_VAISSEAU/2 && event.mouseButton.x <= vaisseau5Sprite.getPosition().x+TAILLE_VAISSEAU/2 && event.mouseButton.y >= vaisseau5Sprite.getPosition().y-TAILLE_VAISSEAU && event.mouseButton.y <= vaisseau5Sprite.getPosition().y+TAILLE_VAISSEAU/2)
                    {
                        joueur.vaisseau.id = 5;
                        strcpy(joueur.vaisseau.nom, NOM_VAISSEAU[4]);
                        vaisseau5Sprite.setTexture(vaisseau5Choice);
                    }
                    else if(event.mouseButton.x >= validationButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= validationButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= validationButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= validationButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseauSprite.getPosition().x, vaisseauSprite.getPosition().y - vaisseauSprite.getLocalBounds().height/1.4);
                    }
                }
                else if (menu == 4) // SI ON EST DANS LE MENU SCORES
                {
                    if(event.mouseButton.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseauSprite.getPosition().x, vaisseauSprite.getPosition().y - vaisseauSprite.getLocalBounds().height/1.4);
                    }
                }
                else if (menu == 5) // SI ON EST DANS LE MENU CHOIX DIFFICULTE
                {
                    if(event.mouseButton.x >= backButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= backButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= backButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= backButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        menu = 1;
                        switch(joueur.vaisseau.id)
                        {
                        case 1:
                            vaisseauSprite.setTexture(vaisseau1);
                            vaisseau1Sprite.setTexture(vaisseau1Choice);
                            break;
                        case 2:
                            vaisseauSprite.setTexture(vaisseau2);
                            vaisseau2Sprite.setTexture(vaisseau2Choice);
                            break;
                        case 3:
                            vaisseauSprite.setTexture(vaisseau3);
                            vaisseau3Sprite.setTexture(vaisseau3Choice);
                            break;
                        case 4:
                            vaisseauSprite.setTexture(vaisseau4);
                            vaisseau4Sprite.setTexture(vaisseau4Choice);
                            break;
                        case 5:
                            vaisseauSprite.setTexture(vaisseau5);
                            vaisseau5Sprite.setTexture(vaisseau5Choice);
                            break;
                        }
                        nomVaisseauText.setPosition(vaisseauSprite.getPosition().x, vaisseauSprite.getPosition().y - vaisseauSprite.getLocalBounds().height/1.4);
                    }
                    else if(event.mouseButton.x >= difficulte1Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseButton.x <= difficulte1Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseButton.y >= difficulte1Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseButton.y <= difficulte1Sprite.getPosition().y+TAILLE_ROND/2)
                    {
                        difficulte = 1;
                        bonusPresent == false;
                        menu = 99;
                        clock.restart();
                        invincibilite.restart();
                        apparitionBonus.restart();
                    }
                    else if(event.mouseButton.x >= difficulte2Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseButton.x <= difficulte2Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseButton.y >= difficulte2Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseButton.y <= difficulte2Sprite.getPosition().y+TAILLE_ROND/2)
                    {
                        difficulte = 2;
                        menu = 99;
                        clock.restart();
                        invincibilite.restart();
                        apparitionBonus.restart();
                    }
                    else if(event.mouseButton.x >= difficulte3Sprite.getPosition().x-TAILLE_ROND/2 && event.mouseButton.x <= difficulte3Sprite.getPosition().x+TAILLE_ROND/2 && event.mouseButton.y >= difficulte3Sprite.getPosition().y-TAILLE_ROND/2 && event.mouseButton.y <= difficulte3Sprite.getPosition().y+TAILLE_ROND/2)
                    {
                        difficulte = 3;
                        menu = 99;
                        clock.restart();
                        invincibilite.restart();
                        apparitionBonus.restart();
                    }
                }
                else if(menu == 99 && gameover == 1)
                {
                    if(event.mouseButton.x >= returnToMenuButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= returnToMenuButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= returnToMenuButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= returnToMenuButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        gameover = 0;
                        menu = 1;
                    }
                    else if(event.mouseButton.x >= playAgainButtonImage.getPosition().x-LARGEUR_BOUTON/2+VIDE_BUTTON && event.mouseButton.x <= playAgainButtonImage.getPosition().x+LARGEUR_BOUTON/2-VIDE_BUTTON && event.mouseButton.y >= playAgainButtonImage.getPosition().y-HAUTEUR_BOUTON/2 && event.mouseButton.y <= playAgainButtonImage.getPosition().y+HAUTEUR_BOUTON/2)
                    {
                        window.close();
                    }
                }
            }
        }

        window.clear();

        // Sprites drawing

        window.draw(backgroundSprite);
        window.draw(logoSprite);

        if(menu == 1) // SI ON EST DANS LE MENU PRINCIPAL
        {
            window.draw(startButtonImage);
            window.draw(optionsButtonImage);
            window.draw(quitButtonImage);
            window.draw(startText);
            window.draw(optionsText);
            window.draw(quitText);
            window.draw(vaisseauSprite);
            window.draw(nomVaisseauText);
            window.draw(trophySprite);
        }
        else if(menu == 2) // SI ON EST DANS LE MENU OPTIONS
        {
            window.draw(backButtonImage);
            window.draw(backText);
            window.draw(barSprite);
            window.draw(volumeChoiceText);

            // Mettre à jour le volume et le centrer
            char str[3];
            sprintf(str, "%i", volume);

            volumeNumberText.setString(str);
            FloatRect bounds = volumeNumberText.getLocalBounds();
            volumeNumberText.setOrigin(bounds.width/2, bounds.height);

            window.draw(volumeNumberText);
            window.draw(backSprite);
            window.draw(forwardSprite);

        }
        else if(menu == 3) // SI ON EST DANS LE MENU CHOIX VAISSEAU
        {
            window.draw(vaisseau1Sprite);
            window.draw(vaisseau2Sprite);
            window.draw(vaisseau3Sprite);
            window.draw(vaisseau4Sprite);
            window.draw(vaisseau5Sprite);
            window.draw(choixVaisseauText);
            window.draw(validationButtonImage);
            window.draw(validationText);
            window.draw(nomVaisseauText);

            switch(joueur.vaisseau.id)
            {
            case 1:
                vaisseau1Sprite.setTexture(vaisseau1Choice);
                break;
            case 2:
                vaisseau2Sprite.setTexture(vaisseau2Choice);
                break;
            case 3:
                vaisseau3Sprite.setTexture(vaisseau3Choice);
                break;
            case 4:
                vaisseau4Sprite.setTexture(vaisseau4Choice);
                break;
            case 5:
                vaisseau5Sprite.setTexture(vaisseau5Choice);
                break;
            }
        }

        else if(menu == 4) // SI ON EST DANS LE MENU SCORES
        {
            window.draw(backButtonImage);
            window.draw(backText);
            window.draw(scoresText);

            // Mettre à jour le score 1 et le centrer
            char str[20];
            sprintf(str, "[1] %i", scores[0]);

            score1Text.setString(str);
            FloatRect bounds = score1Text.getLocalBounds();
            score1Text.setOrigin(bounds.width/2, bounds.height);

            window.draw(score1Text);

            // Mettre à jour le score 2 et le centrer
            char str2[20];
            sprintf(str2, "[2] %i", scores[1]);

            score2Text.setString(str2);
            FloatRect bounds2 = score2Text.getLocalBounds();
            score2Text.setOrigin(bounds2.width/2, bounds2.height);

            window.draw(score2Text);

            // Mettre à jour le score 3 et le centrer
            char str3[20];
            sprintf(str3, "[3] %i", scores[2]);

            score3Text.setString(str3);
            FloatRect bounds3 = score3Text.getLocalBounds();
            score3Text.setOrigin(bounds3.width/2, bounds3.height);

            window.draw(score3Text);
        }
        else if (menu == 5) // SI ON EST DANS LE MENU CHOIX DIFFICULTE
        {
            window.draw(difficulteChoiceText);
            window.draw(backButtonImage);
            window.draw(backText);
            window.draw(difficulte1Sprite);
            window.draw(difficulte2Sprite);
            window.draw(difficulte3Sprite);
            window.draw(difficulte1Text);
            window.draw(difficulte2Text);
            window.draw(difficulte3Text);
        }
        else if (menu == 99) // SI ON EST DANS LE JEU
        {

            // Mettre à jour la diffulté et la centrer
            char str[12];
            sprintf(str, "%i", difficulte);

            difficulteNumberText.setString(str);
            FloatRect bounds = difficulteNumberText.getLocalBounds();
            difficulteNumberText.setOrigin(bounds.width/2, bounds.height);

            // Mettre à jour le score et le centrer
            char str2[12];
            sprintf(str2, "%i", score);

            scoreNumberText.setString(str2);
            FloatRect bounds2 = scoreNumberText.getLocalBounds();
            scoreNumberText.setOrigin(bounds2.width/2, bounds2.height);

            // Draw the sprite
            window.draw(inGamebackgroundSprite);
            window.draw(titleText);
            window.draw(difficulteText);
            window.draw(levelRoundSprite);
            window.draw(difficulteNumberText);
            window.draw(scoreText);
            window.draw(scoreBoxSprite);
            window.draw(scoreNumberText);
            window.draw(viesText);

            switch(vies)
            {
            case 0:
                window.draw(starEmpty1);
                window.draw(starEmpty2);
                window.draw(starEmpty3);
                break;
            case 1:
                window.draw(star1);
                window.draw(starEmpty2);
                window.draw(starEmpty3);
                break;
            case 2:
                window.draw(star1);
                window.draw(star2);
                window.draw(starEmpty3);
                break;
            case 3:
                window.draw(star1);
                window.draw(star2);
                window.draw(star3);
                break;
            }

            window.draw(bonusText);
            window.draw(bonus1RoundSprite);
            window.draw(bonus2RoundSprite);
            window.draw(bonus3RoundSprite);

            if(bonus[0] == 1)
                window.draw(bonus1Sprite);
            else
                window.draw(bonus1SpriteEmpty);

            if(bonus[1] == 1)
                window.draw(bonus2Sprite);
            else
                window.draw(bonus2SpriteEmpty);

            if(bonus[2] == 1)
                window.draw(bonus3Sprite);
            else
                window.draw(bonus3SpriteEmpty);


            if (vies != 0)
            {
                bonusRalenti(&bonus1,&clockBonus1,&vitesseV,bonus,bonus2);
                bonusBombe(simple, nbProjectile, bonus, posVaisseau);
                bonusVitesse(&bonus2,&clockVitesse,&vitesseV,bonus,bonus1);
                mouvement(&posVaisseau,vitesseV);
                updateScore(difficulte, clock, &score);
                if (clock.getElapsedTime().asSeconds() >= (nbProjectile+1) * ECART_SIMPLE)
                {
                    do
                    {
                        creationProjectileSimple(nbProjectile, simple);
                    }
                    while (simple[nbProjectile].position.x >= posVaisseau.x - TAILLE_VAISSEAU/2 - 25 && simple[nbProjectile].position.x <= posVaisseau.x + TAILLE_VAISSEAU/2 + 25 &&
                            simple[nbProjectile].position.y <= posVaisseau.y - TAILLE_VAISSEAU/2 - 25 && simple[nbProjectile].position.y >= posVaisseau.y + TAILLE_VAISSEAU/2 + 25);
                    nbProjectile++;
                }

                for (int i = 0; i < nbProjectile; i++)
                {
                    affichageProjectileSimple (i, vies, difficulte, bonus1, simple, clock, &projectileSimple, &imageProjectileSimpleRouge, &imageProjectileSimpleBleu);
                    window.draw(projectileSimple);
                }

                creationBonus(&bonusPresent, &bonusType, &apparitionBonus, &bonusSprite, &bonus1Time, &bonus2Bomb, &bonus3Speed);
                if (bonusPresent == true && apparitionBonus.getElapsedTime().asSeconds() > 5)
                    window.draw(bonusSprite);
                bonusRecup (&bonusPresent, bonusType, bonus, vaisseau, bonusSprite, apparitionBonus);

                vaisseau.setPosition(posVaisseau.x,posVaisseau.y);
                checkCollision(nbProjectile, &vies, simple, posVaisseau, clock, &invincibilite);
                if (invincibilite.getElapsedTime().asSeconds() < 2 && clock.getElapsedTime().asSeconds() > 2 && (std::fmod(invincibilite.getElapsedTime().asSeconds(), 0.25)) < 0.125)
                    switch(joueur.vaisseau.id)
                    {
                    case 1:
                        vaisseau.setTexture(vaisseau1Dark);
                        break;
                    case 2:
                        vaisseau.setTexture(vaisseau2Dark);
                        break;
                    case 3:
                        vaisseau.setTexture(vaisseau3Dark);
                        break;
                    case 4:
                        vaisseau.setTexture(vaisseau4Dark);
                        break;
                    case 5:
                        vaisseau.setTexture(vaisseau5Dark);
                        break;
                    }
                else
                    switch(joueur.vaisseau.id)
                    {
                    case 1:
                        vaisseau.setTexture(vaisseau1);
                        break;
                    case 2:
                        vaisseau.setTexture(vaisseau2);
                        break;
                    case 3:
                        vaisseau.setTexture(vaisseau3);
                        break;
                    case 4:
                        vaisseau.setTexture(vaisseau4);
                        break;
                    case 5:
                        vaisseau.setTexture(vaisseau5);
                        break;
                    }
                window.draw(vaisseau);
            }

            if (vies == 0)
            {
                gameover = 1;
                for (int i = 0; i < nbProjectile; i++)
                {
                    affichageProjectileSimple (i, vies, difficulte, bonus1, simple, clock, &projectileSimple, &imageProjectileSimpleRouge, &imageProjectileSimpleBleu);
                    window.draw(projectileSimple);
                }
                window.draw(vaisseau);


                window.draw(gameOverText);

                // Mettre ・jour le score et le centrer
                char str[12];
                sprintf(str, "%i", score);
                if (score > scores[0])
                {
                    scores[2] = scores[1];
                    scores[1] = scores[0];
                    scores[0] = score;
                }
                else if (score > scores[1] && score != scores[0])
                {
                    scores[2] = scores[1];
                    scores[1] = score;
                }
                else if (score > scores[2] && score != scores[0] && score != scores[1])
                    scores[2] = score;

                gameOverScoreText.setString(str);
                FloatRect bounds = gameOverScoreText.getLocalBounds();
                gameOverScoreText.setOrigin(bounds.width/2, bounds.height);

                window.draw(gameOverScoreText);
                window.draw(returnToMenuButtonImage);
                window.draw(returnToMenuText);
                window.draw(playAgainButtonImage);
                window.draw(playAgainText);
            }

        }
        window.display();
    }

    return 0;
}
